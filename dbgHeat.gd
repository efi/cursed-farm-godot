extends RichTextLabel

func _process(_delta):
	clear()
	add_text("wood: %s" % str(Sensory.sense(&"bonfire_wood")))
	newline()
	add_text("heat: %s" % str(Sensory.sense(&"bonfire_heat")))
