@tool
class_name CheekyExpression extends RefCounted

static var _cheeky_coll: RegEx = RegEx.create_from_string(r"(^|\s)(?<collection>\b[a-zA-Z_]\w+\b)(\.(?<item>\b[a-zA-Z_]\w+\b))?($|\s)")
static var _float: RegEx = RegEx.create_from_string(r"[-+]?\d+(\.\d+)?([eE][+-]?\d+)?")

static func parse_address(s: String) -> Dictionary:
	var r = _cheeky_coll.search(s)
	if r == null: push_error(&"Wrong address «%s» in expression." % s)
	var c = r.get_string(&"collection") as StringName
	var i = r.get_string(&"item") as StringName
	return {&"collection": c, &"item": i}

static func parse(s: String):
	#print(&"Parsing «%s»" % s)
	var l = s.split(&" ", false)
	#print(l)
	var prev_datum = null
	var next_datum = null
	var is_op := false
	for i in len(l):
		#print(&"%s/%s" % [i + 1, len(l)])
		is_op = l[i] in [&"==", &">=", &"<=", &">", &"<", &"??"]
		if prev_datum == null and is_op:
			push_error(&"Infix operator «%s» without a prefix expression." % l[i])
		if next_datum == null:
			if l[i] != &"??":
				if is_op:
					if i == len(l) - 1:
						push_error(&"Infix operator «%s» without a postfix expression." % l[i])
					else:
						if _float.search(l[i + 1]) == null:
							if l[i + 1] == &"date()":
								next_datum = CheekyExpression.d
							elif &"." in l[i + 1]:
								var a = parse_address(l[i + 1])
								next_datum = CheekyExpression.ic.bind(a.collection, a.item)
							else:
								push_error(&"Can't understand item «%s» in postfix position." % l[i + 1])
						else:
							next_datum = l[i + 1] as float
		#printt(prev_datum, next_datum)
		match l[i]:
			&"==":
				return CheekyExpression.op.bind(prev_datum, next_datum, CheekyExpression.eq)
			&">=":
				return CheekyExpression.op.bind(prev_datum, next_datum, CheekyExpression.gte)
			&"<=":
				return CheekyExpression.op.bind(prev_datum, next_datum, CheekyExpression.lte)
			&">":
				return CheekyExpression.op.bind(prev_datum, next_datum, CheekyExpression.gt)
			&"<":
				return CheekyExpression.op.bind(prev_datum, next_datum, CheekyExpression.lt)
			&"date()":
				prev_datum = CheekyExpression.d
			&"??":
				return CheekyExpression.e.bind(prev_datum)
			_:
				if _float.search(l[i]) == null:
					if &"." in l[i]:
						var a = parse_address(l[i])
						prev_datum = CheekyExpression.ic.bind(a.collection, a.item)
					else:
						prev_datum = l[i] as StringName
				else:
					prev_datum = l[i] as float
					
				
	#s = _cheeky_event.sub(s, &'_event(&"$ev")', true)
	#s = _cheeky_date.sub(s, &'_date()', true)
	#s = _cheeky_coll.sub(s, &'_item_count(&"$collection", &"$item")', true)
	#for r in _cheeky_coll.search_all(s):
		#var a = parse_address(r.get_string())
		#s = s.replace(r.get_string(), &'CheekyExpression._item_count(&"%s", &"%s")' % [a.collection, a.item])
	#s = _eqr.sub(s, &"_eq($1, $2)", true)
	#s = _lter.sub(s, &"_lte($1, $2)", true)
	#s = _gter.sub(s, &"_gte($1, $2)", true)
	#s = _ltr.sub(s, &"_lt($1, $2)", true)
	#s = _gtr.sub(s, &"_gt($1, $2)", true)
	#s = _orr.sub(s, &"_or($1, $2)", true)
	#for i in r:
		#prints(&"» ", i)
	#print(&"Result: %s" % r)

static func ic(c: StringName, k: StringName) -> float:
	return Patron.item_count(c, k)

static func d() -> float:
	return Gnomon.current_instance.date

static func e(s: StringName) -> bool:
	return Patron.is_event_complete(s)

static func eq(a, b) -> bool:
	return a == b

static func gt(a, b) -> bool:
	return a > b

static func lt(a, b) -> bool:
	return a < b

static func gte(a, b) -> bool:
	return a >= b

static func lte(a, b) -> bool:
	return a <= b

static func op(a, b, _op):
	var A = a.call() if a is Callable else a
	var B = b.call() if b is Callable else b
	return _op.call(A, B)
