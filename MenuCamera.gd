extends Camera2D

@export_range(1.0, 30.0, 1.0) var speed: float = 10.0

func _enter_tree() -> void:
	await get_viewport().size_changed
	position = get_viewport_rect().size * 0.5

func _process(_delta: float) -> void:
	if Input.is_action_pressed("intent_west"):
		position += Vector2.LEFT * speed
	if Input.is_action_pressed("intent_east"):
		position += Vector2.RIGHT * speed
	if Input.is_action_pressed("intent_north"):
		position += Vector2.UP * speed
	if Input.is_action_pressed("intent_south"):
		position += Vector2.DOWN * speed
