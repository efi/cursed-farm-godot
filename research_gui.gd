extends MarginContainer
var button = preload("res://templates/research_button.tscn")
var ref:
	set(v):
		GUI.beep.play()
		ref = v
#@onready var list = get_node(^"HBoxContainer/ScrollContainer/VBoxContainer")
@onready var requirements = get_node(^"%requirements")
@onready var req_side = get_node(^"%req_side")

func _ready():
	Timing.start(&"research ready")
	for E in Patron.research_names.entries:
		var b: FastButton = button.instantiate()
		b.res = E.key_name
		b.pressed.connect(set.bind(&"ref", b.res))
		%List.add_child(b)
		b.hide()
	%List.reset_size()
	#for res in Gnomon.research:
		#if res in Patron.research_names:
			#var b = button.instantiate()
			#list.add_child(b)
			#b.res = res
			#b.pressed.connect(set.bind(&"ref", res))
	#list.reset_size()
	Timing.check(&"research ready")

func try_research():
	if ref != null:
		var pr = Patron.find_item(&"price", ref, &"research")
		if Patron.can_pay(pr):
			Patron.complete_research_level(ref, Gnomon.current_instance.research[ref].level + 1)
			
			req_side.hide()
			GUI.beep.play()

func _process(_delta):
	if Session.is_active():
		for b in %List.get_children():
			if Patron.collection_has_key(&"research", b.res):
				var Rreq = Patron.find_item(&"conditions", b.res, &"research")
				var Rok: bool = Rreq.check() if Rreq != null else false
				if (Gnomon.current_instance.research[b.res].level > 0 or Rok):
					b.show()
					if ref != null:
						if Patron.can_pay(Patron.find_item(&"price", b.res, &"research"), Gnomon.current_instance.research[b.res].level):
							req_side.find_child("Research").disabled = false
						else:
							req_side.find_child("Research").disabled = true
