class_name Realizable extends Object

var manual: Array = []
var auto: float = 0.0
var key_name: StringName
var instance: Callable

func _init(key_n: StringName, instantiator = null):
	key_name = key_n
	if instantiator != null:
		instance = instantiator

func manual_add(i) -> void:
	manual.push_back(i)

func auto_add(n: float) -> void:
	auto += n

func realize():
	if instance == null:
		push_error(&"Realizable instance is null on %s", key_name)
	if auto >= 1.0:
		auto -= 1.0
		manual.push_back(instance.call())
		return

func current_value() -> float:
	return len(manual) + auto

func find(id: int, cond: Callable = func(i): return i.UID == id) -> Variant:
	for m in manual:
		if cond.call(m):
			return m
	return null

func serialize():
	var ser_man = ""
	for i in manual:
		ser_man += i.serialize()
	return &"%s+%s\n" % [auto, ser_man]
