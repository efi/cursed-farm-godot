class_name ModifierStack extends RefCounted

static var _stack: Array[GameModifier] = []

static func find(m: GameModifier) -> int:
	for GM in _stack:
		if GM.kind == m.kind:
			return _stack.find(GM)
	return -1

static func add(m: GameModifier):
	var o: int = find(m)
	var acts = {}
	if o != -1:
		acts = _stack[o].actions()
		match _stack[o].stacks():
			ModifierTemplate.STACKING.UPDATE:
				_stack[o] = m
			ModifierTemplate.STACKING.TIME:
				_stack[o].time += m.time
			ModifierTemplate.STACKING.POWER:
				_stack[o].amount += 1
			ModifierTemplate.STACKING.IGNORE:
				pass
		if &"action_start" in acts:
			acts.action_start.run()
	else:
		acts = m.actions()
		_stack.push_back(m)
	if &"action_add" in acts:
		acts.action_add.run()

static func remove(m: GameModifier):
	var o: int = find(m)
	if o == -1:
		return
	var acts = _stack[o].actions()
	if &"action_remove" in acts:
		acts.action_remove.run()
	match _stack[o].removes():
		ModifierTemplate.REMOVING.ONE:
			_stack[o].amount -= 1
		ModifierTemplate.REMOVING.ONE:
			_stack[o].amount = 0
		ModifierTemplate.REMOVING.FUNC:
			_stack[o].amount = _stack[o].removal_fn().run()
	if _stack[o].amount == 0:
		if &"action_end" in acts:
			acts.action_end.run()
		_stack.remove_at(o)

static func contains(n: StringName) -> bool:
	for GM in _stack:
		if GM.kind == n:
			return true
	return false

static func locate(n: StringName) -> int:
	for gmi in len(_stack):
		if _stack[gmi].kind == n:
			return gmi
	return -1

static func count(n: StringName) -> int:
	var i := locate(n)
	if i != -1:
		return _stack[i].amount
	return 0

static func category(c: StringName) -> Array[GameModifier]:
	if c.is_empty():
		return []
	var r = [] as Array[GameModifier]
	for GM in _stack:
		if c in GM.categories():
			r.push_back(GM)
	return r
