class_name GameEvent extends Resource

@export var name: StringName
@export var cond: RequirementSet
@export var result: Spark

func check():
	if Gnomon.current_instance != null:
		if name not in Gnomon.current_instance.game_events and cond.check():
			result.run_script()
			Gnomon.current_instance.complete_event(name)
