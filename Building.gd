class_name Building extends UIDCollectable

var kind: StringName
var region: Vector2i
var location: int
var slots: int
var contents: Array

func _init(t: BuildingTemplate, at: Vector2i, on: int):
	super()
	kind = t.name
	region = at
	location = on
	slots = t.base_slots
	contents.resize(slots)
