extends Label
const tempwords = {
	&"No fire": 0,
	&"Warm ashes": 1,
	&"Smoldering ashes": 10,
	&"Burning ashes": 50,
	&"Small fire": 100,
	&"Burning fire": 200,
	&"Roaring fire": 500,
	&"Infernal fire": 2000,
	&"Collosal fire": 10000,
	&"Absurd fire": 30000
}

func _process(_delta):
	if Session.is_active():
		if Patron.is_research_level_complete(&"bonfire"):
			var h: float = Sensory.sense(&"bonfire_heat")
			if h < 0 or is_nan(h):
				text = &"Error fire"
			else:
				var w := &""
				for nw in tempwords:
					if tempwords[nw] > h:
						text = w
						break
					else:
						w = nw
