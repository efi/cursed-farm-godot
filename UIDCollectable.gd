class_name UIDCollectable extends RefCounted

static var UIDS: Dictionary = {}
var UID: int

func _init(recreate = false):
	if not recreate:
		UID = Patron.UID()
		UIDCollectable.UIDS[UID] = self
	#Gnomon.UIDs[UID] = self
