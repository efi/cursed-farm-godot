class_name Sensory extends Object

static var _last_update: float = 0.0
static var _sensors: Dictionary = {}

static func update(new_date: float) -> void:
	_last_update = new_date
	for s in _sensors:
		_sensors[s].sense(s)

static func add_sensor(n: StringName, f: Callable) -> void:
	_sensors[n] = Sensor.new(f)

static func sense(v):
	if v not in _sensors: push_warning(&"Sensor «%s» not found." % v)
	assert(v in _sensors)
	return _sensors[v].sense(v)

class Sensor extends Object:
	var fn: Callable
	var last_update: float = 0.0
	var value: float = 0.0

	func _init(f: Callable):
		fn = f

	func sense(x):
		if last_update < Sensory._last_update:
			last_update = Sensory._last_update
			if not fn.is_valid(): push_error(&"Sensor error. %s fn is invalid." % x)
			value = fn.call()
		return value
