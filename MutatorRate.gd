class_name MutatorRate extends RefCounted

var kind
var item
var amount
var mod

func _init(k: StringName, i: StringName, a: float, m: float):
	kind = k
	item = i
	amount = a
	mod = m
