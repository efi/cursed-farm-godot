class_name Pattern2D extends RefCounted

static var directions := [&"E", &"N", &"W", &"S"]
static var flips := [&"N", &"H", &"V"]

var _values := {}

func _tr(p: Vector2i, f: StringName, r: StringName) -> Vector2i:
	match f:
		&"N":
			pass
		&"H":
			p = Vector2i(-p.x, p.y)
		&"V":
			p = Vector2i(p.x, -p.y)
	match r:
		&"E":
			pass
		&"N":
			p = Vector2i(p.y, -p.x)
		&"W":
			p = Vector2i(-p.x, -p.y)
		&"S":
			p = Vector2i(-p.y, p.x)
	return p

func check(on: PackedInt32Array, ref: Dictionary, i: int, w: int, h: int = w) -> Array:
	var pos := GridLimit.idx2point(i, w, h)
	var res := []
	for fl in flips:
		for ro in directions:
			var valid = true
			for v in _values:
				var at := GridLimit.point2idx(pos + _tr(v, fl, ro), w, h)
				#if at != -1: printt(i, pos, v, fl, ro, at)
				if at == -1 or ref[_values[v]] != on[at]:
					valid = false
					break
			if valid:
				res.push_back([i, fl, ro])
	return res

func positions(f: StringName, r: StringName) -> PackedVector2Array:
	var res: PackedVector2Array = []
	for v in _values:
		res.push_back(_tr(v, f, r))
	return res

func _init(data: String):
	#coords relative to the pattern origin
	var x := 0
	var y := 0
	for r in data.split(&";"):
		for s in r.split(&" "):
			if s != &"_":
				_values[Vector2i(x, y)] = s
			x += 1
		x = 0
		y += 1
