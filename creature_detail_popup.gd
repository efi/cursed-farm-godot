extends MarginContainer

@onready var L_dim = find_child(&"Dimension")
@onready var L_reg = find_child(&"Region")
@onready var L_bui = find_child(&"Building")
@onready var L_cre = find_child(&"Creature")
@onready var list = find_child(&"creature_list")
var row = preload("res://templates/creature_display.tscn")
var building: StringName:
	set(v):
		building = v
		update()
		return true

func _notification(what):
	if what == NOTIFICATION_VISIBILITY_CHANGED:
		if GUI.beep != null:
			GUI.beep.play()

func update():
	L_dim.text = Gnomon.current_instance.dimension_name[GUI.MapGUI.current_display[0]]
	L_reg.text = Gnomon.current_instance.region_name[GUI.MapGUI.current_display]
	L_bui.text = Patron.find_item(&"plural", building, &"display_names") #.display_names.plural[building]
	L_cre.text = &""
	for c in list.get_children():
		list.remove_child(c)
		c.queue_free()
	for b in Gnomon.current_instance.buildings[building].manual:
		var p = PanelContainer.new()
		var v = VBoxContainer.new()
		var l = Label.new()
		list.add_child(p)
		p.add_child(v)
		v.add_child(l)
		l.text = Patron.find_item(&"singular", building, &"display_names") #.display_names.singular[building]
		for cid in b.contents:
			if cid != null:
				var c = UIDCollectable.UIDS[cid]
				var r = row.instantiate()
				v.add_child(r)
				r.icon.icon = c.kind
				r.L_type.text = Patron.find_item(&"singular", c.kind, &"display_names") #.display_names.singular[c.kind]
				r.L_name.text = c.name
				r.L_dlc.text = c.hat
				#r.L_work.text = Patron.display_names.singular[b.kind]
