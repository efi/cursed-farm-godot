class_name GUI extends RefCounted
static var Main: Node
static var Cursor: Node
static var Tooltip: Node
static var Game: Node
static var Status: Node
static var Resources: Node
static var MapGUI: Node
static var ResearchGUI: Node
static var Bonfire: Node
static var Cauldron: Node
static var Creatures: Node
static var Mines: Node
static var Pantheon: Node
static var Alert: Node
static var Log: Node
static var MainMenu: Node
static var GameMenu: Node
static var DLCPopup: Node
static var QuitPopup: Node
static var Tabs: Node
static var Fadescreen: Node
static var beep: AudioStreamPlayer
static var beep_high: AudioStreamPlayer
static var beep_error: AudioStreamPlayer

static func main(m: Node) -> void:
	Main = m
	DLCPopup = Main.get_node(^"%DLC Popup")
	QuitPopup = Main.get_node(^"%Quit popup")
	MainMenu = Main.get_node(^"%Main Menu")
	GameMenu = Main.get_node(^"%Game Menu")
	Tooltip = Main.get_node(^"%Tooltip")
	Fadescreen = Main.get_node(^"%Fadescreen")
	beep = Main.get_node(^"%beep")
	beep_high = Main.get_node(^"%beep_high")
	beep_error = Main.get_node(^"%beep_error")
	
static func fetch(game: Node) -> void:
	Game = game
	Status = Game.get_node(^"%Status")
	Resources = Game.get_node(^"%Resources view")
	MapGUI = Game.get_node(^"%Map")
	ResearchGUI = Game.get_node(^"%Research")
	Bonfire = Game.get_node(^"%Bonfire")
	Cauldron = Bonfire.get_node(^"%Cauldron")
	Creatures = Game.get_node(^"%Creatures")
	Mines = Game.get_node(^"%Mines")
	Pantheon = Game.get_node(^"%Pantheon")
	Alert = Game.get_node(^"%Alert/Label")
	Log = Game.get_node(^"%Log/MarginContainer/ScrollContainer/VBoxContainer")
	Tabs = Game.get_node(^"%Middle/TabContainer")
