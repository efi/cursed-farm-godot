@tool
class_name Constantly extends RefCounted

static var _fn = func(v): return v

static func __(v: Variant) -> Callable:
	return _fn.bind(v)
