class_name Price extends Resource

@export var price: Dictionary


#func can_pay() -> bool:
	#for k in price:
		#if Patron.find(k) < price[k]:
			#return false
	#return true
#
#func pay() -> void:
	#assert(can_pay(), "Couldn't pay! Check before or die trying")
	#for k in price:
		#Patron.fset(k, Patron.find(k) - price[k])
#
func _to_string():
	return str(&"Price: %s" % price)
