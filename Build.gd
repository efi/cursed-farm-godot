extends VBoxContainer
var build_mode = false
var which
@onready var button = get_node("Build Button")
@onready var grid = get_node("ScrollContainer/BuildGrid")

func _ready():
	for i: Entry in Patron.buildings.entries:
		if &"no_build" in i.values.category: continue
		var b = Button.new()
		b.custom_minimum_size = Vector2(48, 48)
		b.name = i.key_name
		var loc = Patron.locate_sprite(i.key_name)
		if loc:# i.key_name in Patron.sprites:
			b.icon = Patron.sprite(loc, i.key_name)
			b.expand_icon = true
		b.toggle_mode = true
		b.pressed.connect(enable_build_mode.bind(i.key_name))
		b.mouse_entered.connect(hover.bind(b))
		b.mouse_exited.connect(unhover)
		grid.add_child(b)
		b.hide()

func hover(me):
	GUI.Tooltip.update(me.name, me.name, Patron.building_ts[me.name].price)
	GUI.Tooltip.position = me.get_global_rect().position + Vector2(0, -(4 + GUI.Tooltip.size.y))
	GUI.Tooltip.show()

func unhover():
	GUI.Tooltip.hide()

func toggle_build_mode():
	if build_mode: disable_build_mode()
	else: enable_build_mode(which)

func enable_build_mode(w):
	GUI.beep.play()
	if w == null:
		for c in grid.get_children():
			if c.visible:
				w = c.name
				break
	which = str(w)
	build_mode = true
	button.set_pressed_no_signal(true)
	reset_buttons()
	grid.get_node(which).set_pressed_no_signal(true)
	for c in GUI.MapGUI.Map_display.get_children():
		c.forbidden_outline(which)

func disable_build_mode():
	if build_mode: GUI.beep.play()
	build_mode = false
	GUI.MapGUI.context.update(-1)
	button.set_pressed_no_signal(false)
	reset_buttons()
	for c in GUI.MapGUI.Map_display.get_children():
		c.forbidden_outline()

func reset_buttons():
	for c in grid.get_children():
		c.set_pressed_no_signal(false)

func update_visible():
	if Session.is_active():
		for c in grid.get_children():
			if Patron.is_research_level_complete(c.name): c.show()
			else: c.hide()
			if c.name == &"bonfire":
				c.disabled = not Patron.is_research_level_complete(&"bonfire", int(Patron.item_count(&"buildings", &"bonfire")))
		if button.disabled and grid.get_children().any(func(i): return i.visible):
			button.disabled = false

func _process(_delta):
	update_visible()
