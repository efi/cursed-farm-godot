class_name BuildingTemplate extends Object

var name: StringName
var category: PackedStringArray
var allow_on: PackedStringArray
var allow_in: PackedStringArray
var price: Price
var base_slots: int
var construction_time: float

func _init(n: StringName, c: PackedStringArray, aon: PackedStringArray, ain: PackedStringArray, bs: int, p: Price, ct: float):
	name = n
	category = c
	allow_on = aon
	allow_in = ain
	base_slots = bs
	price = p
	construction_time = ct

func can_pay():
	return Patron.can_pay(price)

func pay():
	Patron.pay_level(price)

func can_build(on_tile) -> bool:
	return on_tile in allow_on or Patron.collection_has_key(&"collectables", on_tile)
