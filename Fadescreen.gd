extends Control

enum {NO, IN, OUT}
var fading := NO
var _timer: float
var _timex: float
var _color: Color = Color.BLACK
signal done

func fade_out(time: float = 2.0, color = Color.BLACK):
	get_tree().paused = true
	print(&"Fade out")
	if fading != NO:
		push_warning(&"Fading while already fading!")
	_timex = time
	_timer = time
	fading = OUT
	_color = Color(color, 0)
	show()

func fade_in(time: float = 2.0, force := false, color = null):
	print(&"Fade in")
	if color != null and not color is Color:
		push_warning(&"Fade screen found %s is not a color" % color)
	if force or fading == OUT and _timer <= 0.0:
		if color != null and color is Color:
			_color = color
		_timex = time
		_timer = time
		fading = IN
	get_tree().paused = false

func _process(delta):
	if fading != NO:
		queue_redraw()
		_timer -= delta
		if fading == OUT:
			_color = Color(_color, lerp(1.0, 0.0, _timer / _timex))
			if _timer <= 0:
				done.emit()
				return
		if fading == IN:
			_color = Color(_color, lerp(0.0, 1.0, _timer / _timex))
			if _timer <= 0:
				done.emit()
				fading = NO
				hide()

func _draw():
	if fading != NO:
		draw_rect(get_viewport_rect(), _color)
