class_name Patron extends RefCounted

#region Constants
#static var resource_types
const valid_collections: PackedStringArray = [
	&"sprites",
	&"collectables",
	&"buildings",
	&"creatures",
	&"display_names",
	&"research",
	&"research_names",
	&"MapButtonPriority",
	&"potion_components",
	&"potion_recipes",
	&"cauldron",
	&"loglines",
	&"storage",
	#&"curses",
	]
static var sprites
static var collectables: Collection
static var buildings: Collection
static var building_ts: Dictionary
static var creatures: Collection
static var display_names: Collection
static var research_names: Collection
static var research: Collection
#static var research_requirements: Dictionary
static var game_events := {}
static var MapButtonPriority
static var potion_components
static var potion_recipes
static var cauldron
const temperature: Dictionary = {
	&"Deadly cold": -20,
	&"Freezing": 0,
	&"Cold": 10,
	&"Chilly": 15,
	&"Comfortable": 20,
	&"Warm": 25,
	&"Hot": 30,
	&"Scorching": 35,
	&"Burning": 40,
	&"Deadly hot": 600,
	&"Infernal": INF,
}
const astro_name: PackedStringArray = [
	&"Aries",
	&"Taurus",
	&"Gemini",
	&"Cancer",
	&"Leo",
	&"Virgo",
	&"Libra",
	&"Scorpio",
	&"Sagittarius",
	&"Capricorn",
	&"Aquarius",
	&"Pisces"
]
const weather: PackedStringArray = [
	&"Clear",
	&"Sunny",
	&"Cloudy",
	&"Rainy",
	&"Stormy",
	&"Thundering",
	&"Snowy",
	&"Hailing"
]
enum Weather {CLEAR, SUNNY, CLOUDY, RAINY, STORMY, THUNDERING, SNOWY, HAILING}
static var can_build: Dictionary
static var loglines
static var storage = []
static var mutators: Collection
static var curses := {}
const texture_dir = &"res://textures/"

static func price(p: Dictionary = {}):
	var pr = Price.new()
	pr.price = p
	return pr

static func price_template(p: Dictionary = {}):
	var pr = PriceTemplate.new()
	pr.price = p
	return pr

#endregion

#region Helpers
static func UID() -> int:
	return Gnomon.current_instance.UID()

static func num_to_str(n):
	if n == null: return &"#NULL"
	n = floori(n)
	if n == 0: return &"0"
	var l = floori((log(abs(n)) / log(10)) / 3)
	if is_nan(l): return &"0"
	if l <= 7:
		return str(floori(n / pow(1000, l))) + [&"", &"K", &"M", &"G", &"T", &"E", &"Z", &"Y"][l]
	else:
		var l2 = floori(log(abs(n)) / log(10))
		return str(floori(n / pow(10, l2))) + &"e" + str(l2)

static func date_passed(date: float) -> bool:
	return current_date() >= date

static func current_date() -> float:
	if Gnomon.current_instance == null: return 0.0
	return Gnomon.current_instance.date

static func in_collection(c: Collection, key: StringName) -> Entry:
	for e in c.entries:
		if e.key_name == key:
			return e
	return null

static func in_entry(e: Entry, c: StringName):
	if e.values.has(c):
		return e.values[c]
	push_warning(&"No value for key %s in Entry" % c)
	return null

static func locate(item: StringName) -> StringName:
	for c in Patron.valid_collections:
		if collection_has_key(c, item):
			return c
	push_error(&"Unable to locate %s" % item)
	return &""

static func find_item(key: StringName, item: StringName, collection: StringName):
	if collection not in Patron.valid_collections:
		if EngineDebugger.is_active(): push_warning(&"Collection %s not valid" % collection)
		return null
	if not collection_has_key(collection, item):
		if EngineDebugger.is_active(): push_warning(&"Item %s not in collection %s" % [item, collection])
		return null
	return Patron.in_entry(Patron.in_collection(Patron[collection], item), key)

static func collection_has_key(collection: StringName, key: StringName) -> bool:
	if key.is_empty(): return false
	if collection not in Patron.valid_collections:
		push_warning(&"Collection %s not valid" % collection)
		return false
	return in_collection(Patron[collection], key) != null

static func item_count(collection: StringName, item: StringName) -> float:
	if collection == &"workforce":
		return Gnomon.item_count(collection, item)
	if collection == &"free":
		return Gnomon.current_instance.free_creatures(item)
	if collection == &"storage":
		return storage_for(item)
	if collection not in Patron.valid_collections:
		push_warning(&"Collection %s not valid" % collection)
		return 0.0
	return Gnomon.item_count(collection, item)

static func storage_for(item: StringName) -> float:
	var total := 0.0
	for e in Patron.storage.entries:
		if e.values.item == item:
			var l: StringName = locate(e.key_name)
			total += Patron.item_count(l, e.key_name) * e.values.amount
	return total

static func sprite(sub_collec: StringName, spr: StringName):
	if sub_collec.is_empty() or spr.is_empty(): return null
	var a = find_item(&"atlas", sub_collec, &"sprites") #in_entry(e, &"atlas")
	if null == a: push_warning("sprite not found: %s in %s" % [spr, sub_collec])
	#print(&"«%s».«%s»" % [sub_collec, spr])
	if spr in a.sub_textures:
		return a.sub_textures[spr]
	else:
		return null

static func locate_sprite(spr: StringName):
	for c in Patron.sprites.entries:
		if spr in c.values.atlas.sub_textures:
			return c.key_name
	return null

static func parse_range(s: String) -> PackedInt32Array:
	var r: PackedInt32Array = []
	if &"-" in s:
		var sp = s.split(&"-")
		r = [sp[0] as int, sp[1] as int]
	else:
		r = [s as int, s as int]
	return r

static func temperature_word(t: int) -> StringName:
	for w in temperature:
		if t <= temperature[w]: return w as StringName
	return &"#ERROR"

static func weather_word(w: Patron.Weather) -> StringName:
	return weather[w]

static func map_dimensions() -> PackedInt64Array:
	var r: PackedInt64Array = []
	for v in Gnomon.current_instance.map:
		if not v[0] in r:
			r.push_back(v[0])
	return r

static func get_dimension_name(k) -> StringName:
	return Gnomon.current_instance.dimension_name[k]

static func dimension_regions(n: int) -> Array[Vector2i]:
	var r: Array[Vector2i] = []
	for v in Gnomon.current_instance.map:
		if v[0] == n and not v in r:
			r.push_back(v)
	return r

static func get_region_name(k) -> StringName:
	return Gnomon.current_instance.region_name[k]

static func price_level(p: Price, level: int):
	var rp: Dictionary = {}
	for c in p.price:
		for i in p.price[c]:
			if c not in rp: rp[c] = {}
			rp[c][i] = p.price[c][i] * pow(2, level - 1)
	return rp

static func force_complete_event(name: StringName):
	if name in Patron.game_events:
		Patron.game_events[name].result.run_script()
		#gnomon.game_events[name].completed = true
		Gnomon.current_instance.complete_event(name)

static func is_event_complete(name: StringName) -> bool:
	if name in Gnomon.current_instance.game_events:
		return true
	return false

static func is_research_level_complete(name: StringName, level: int = 1) -> bool:
	if name in Gnomon.current_instance.research:
		return Gnomon.current_instance.research[name].level >= level
	return false

static func get_research_name(name: StringName) -> String:
	return Patron.find_item(&"name", name, &"research_names")

static func get_research_description(name: StringName) -> String:
	return Patron.find_item(&"description", name, &"research_names")

static func get_research_price(name: StringName) -> Price:
	return Patron.find_item(&"price", name, &"research")

static func check_requirements(name: StringName) -> bool:
	return Patron.find_item(&"conditions", name, &"research").check()

static func can_pay(p: Price, level: int = 1) -> bool:
	var lp = price_level(p, level)
	for c in lp:
		for i in lp[c]:
			if Patron.item_count(c, i) < lp[c][i]:
				return false
	return true

static func pay_level(p: Price, level = 1) -> void:
	var lp = price_level(p, level)
	for c in lp:
		for i in lp[c]:
			Gnomon.current_instance.add_resource(c, i, -lp[c][i])

static func complete_research_level(r: StringName, level: int = 1):
	var rp = Patron.find_item(&"price", r, &"research")
	pay_level(rp, level)
	Gnomon.current_instance.add_research(r, level)

static func can_house(c: StringName):
	#print(&"%s - %s = " % [storage_for(c), item_count(&"creatures", c)])
	return storage_for(c) - item_count(&"creatures", c) > 0

static func can_hire(c: Creature):
	if can_pay(c.price) and can_house(c.kind):
		return true
	return false

static func try_hire(c: Creature):
	if can_hire(c):
		pay_level(c.price)
		Gnomon.current_instance.add_resource(&"creatures", c.kind, 1.0, c)
		return true
	return false

static func placeholder(n: StringName) -> Research:
	var rqs = RequirementSet.new()
	var r = Requirement.new()
	r.condition = &"false"
	rqs.reqs = [r]
	rqs.parse_all()
	return Research.new(n, rqs, Price.new())


static func get_hat():
	return &"wearing a funi hat"

static func logline(id):
	return find_item(&"text", id, &"loglines")

#endregion

static func giddy_up():
	Timing.start(&"data loader")
	Gnomon.fresh_instance()
	for file in DirAccess.get_files_at(&"res://game_events/"):
		Patron.game_events[file.get_basename()] = load(&"res://game_events/" + file)
	for gek in Patron.game_events:
		Patron.game_events[gek].cond.parse_all()
	loglines = ResourceLoader.load("res://data/loglines.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	display_names = ResourceLoader.load("res://data/display_names.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	sprites = ResourceLoader.load("res://data/sprites.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	collectables = ResourceLoader.load("res://data/collectables.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	creatures = ResourceLoader.load("res://data/creatures.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	buildings = ResourceLoader.load("res://data/buildings.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	for entry: Entry in buildings.entries:
		building_ts[entry.key_name] = BuildingTemplate.new(
			entry.key_name,
			entry.values.category,
			entry.values.on,
			entry.values.in,
			entry.values.slots,
			entry.values.price as Price,
			entry.values.time)
	research_names = ResourceLoader.load("res://data/research_names.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	research = ResourceLoader.load("res://data/research.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	for E in Patron.research.entries:
		E.values.conditions.parse_all()
	cauldron = ResourceLoader.load("res://data/cauldron_symbols.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	potion_components = ResourceLoader.load("res://data/potion_components.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	potion_recipes = ResourceLoader.load("res://data/potion_recipes.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	storage = ResourceLoader.load("res://data/storage.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	research_names = ResourceLoader.load("res://data/research_names.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	MapButtonPriority = ResourceLoader.load("res://data/MapButtonPriority.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	mutators = ResourceLoader.load("res://data/mutators.data", "Collection", ResourceLoader.CACHE_MODE_REPLACE)
	for E in mutators.entries:
		E.values.requirement.parse_all()
	#curses = data_file(&"curses",
		#func(acc, line):
			#var l = line.split(&" ", 2)
			#if len(l) != 3:
				#return acc
			#var rs: Array[Requirement] = []
			#for r in l[2].split(&","):
				#rs.push_back(Requirement.new(r))
			#acc.merge({l[0]: Curse.new(RequirementSet.new(rs), Sparks.new_spark(l[1]))})
			#return acc)
	Gnomon.current_instance.giddy_up()
	Timing.check(&"data loader")
