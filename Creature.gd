class_name Creature extends UIDCollectable

var kind: StringName
var name: StringName
var age: int
var price: Price
var hat: StringName

func _init(t: CreatureTemplate):
	super()
	kind = t.kind
	name = t.make_name()
	age = t.make_age()
	price = t.make_price()
	hat = t.make_hat()
