extends MarginContainer

@onready var grid = find_child(&"GridContainer")
var initial_gods: Array[Goddess] = [
	Goddess.new(&"Ariel"),
	Goddess.new(&"Becca"),
	Goddess.new(&"Charon"),

	Goddess.new(&"Dani"),
	Goddess.new(&"Eleanor"),
	Goddess.new(&"Funkass"),

	Goddess.new(&"Ganesh"),
	Goddess.new(&"Hanna"),
	Goddess.new(&"Imelda"),

	Goddess.new(&"Janet"),
	Goddess.new(&"Kim"),
	Goddess.new(&"Lemmy"),
]

func _ready():
	for g in initial_gods:
		var b = Button.new()
		b.text = g.name
		b.custom_minimum_size = Vector2(130, 100)
		grid.add_child(b)
