class_name Requirement extends Resource

@export var condition: String
var _expr: Array = []
var _parsed: bool

func parse():
	if not _parsed:
		if condition == &"always":
			_parsed = true
			return
		for e in condition.split(&" or "):
			var r = CheekyExpression.parse(condition)
			if r.is_valid():
				_expr.push_back(r)
			else:
				push_error(&"Requirement: Condition part caused an invalid lambda: %s" % e)
		#print(&"Result expression: %s" % _expr)
		_parsed = true

func check():
	#if null == _expr: parse()
	if condition == &"always": return true
	if not _parsed:
		push_warning(&"Requirement: Expression was not parsed. Condition: %s" % condition)
		parse()
	var r: bool = false
	for i in _expr:
		r = r or i.call()
	return r

func _to_string():
	return str(&"Requirement{%s}" % condition)
