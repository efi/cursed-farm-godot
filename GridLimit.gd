class_name GridLimit extends Object

static func _err(n: int, w: int, h: int):
	if n < 0 or n > w * h:
		push_error("Grid index %s out of bounds" % str(n))

static func flip(i: int, vh: int, w: int, h: int = w) -> int:
	if i == -1:
		return -1
	_err(i, w, h)
	var p := idx2point(i, w, h)
	match vh % 3:
		0:
			return i
		1:
			return point2idx(Vector2i(w - 1 - p.x, p.y), w, h)
		2:
			return point2idx(Vector2i(p.x, h - 1 - p.y), w, h)
		3:
			return point2idx(Vector2i(w - 1 - p.x, h - 1 - p.y), w, h)
		_:
			return -1

static func rotate(i: int, r: int, w: int, h: int = w) -> int:
	if i == -1:
		return -1
	_err(i, w, h)
	var p := idx2point(i, w, h)
	match r % 4:
		0:
			return i
		1:
			return point2idx(Vector2i(p.y, w - 1 - p.x), w, h)
		2:
			return point2idx(Vector2i(w - 1 - p.x, h - 1 - p.y), w, h)
		3:
			return point2idx(Vector2i(h - 1 - p.y, p.x), w, h)
		_:
			return -1

static func idx2point(i: int, w: int, h: int = w) -> Vector2i:
	_err(i, w, h)
	return Vector2i(i % w, floori(i / w))

static func point2idx(p: Vector2i, w: int, h: int = w) -> int:
	if  p.x < 0 or\
		p.y < 0 or\
		p.x >= w  or\
		p.y >= h:
			return -1
	return p.x + p.y * w

static func north(n: int, w: int, h: int = w) -> int:
	_err(n, w, h)
	if n - w < 0:
		return -1
	return n - w

static func south(n: int, w: int, h: int = w) -> int:
	_err(n, w, h)
	if n + w >= w * h:
		return -1
	return n + w

static func west(n: int, w: int, h: int = w) -> int:
	_err(n, w, h)
	if n % w == 0:
		return -1
	return n - 1

static func east(n: int, w: int, h: int = w) -> int:
	_err(n, w, h)
	if n % w == w - 1:
		return -1
	return n + 1

static func at(n: int, x: int, y: int, w: int, h: int = w) -> int:
	if n == -1:
		return -1
	_err(n, w, h)
	var p = idx2point(n, w, h)
	if     p.x + x <  0\
		or p.x + x >= w\
		or p.y + y <  0\
		or p.y + y >= h:
		return -1
	return n + x + y * w
