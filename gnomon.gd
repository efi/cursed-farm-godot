class_name Gnomon extends RefCounted

static var current_instance: Gnomon

@export var save_version: int = 0
@export var last_UID: int = 0
@export var collectables := {}
@export var mutators := {}
@export var creatures := {}
@export var buildings := {}
@export var DLC := {&"hat": false, &"spider": false, &"straight": false}
@export var date := 0.0
@export var temperature := 20.0
@export var humidity := 100.0
@export var weather: Patron.Weather
@export var time_stretch := 1.0
@export var map := {}
@export var dimension_name := {}
@export var region_name := {}
@export var bonfire := {&"wood": 0, &"heat": 0}
@export var visible_resources := []
@export var potions := {}
@export var research := {}
@export var workforce := {}
@export var works_in := {}
@export var game_events: PackedStringArray = []

#static func _static_init():
	#Sensory.add_sensor(&"temperature", func(): return Gnomon.temperature)
	#Sensory.add_sensor(&"humidity", func(): return Gnomon.humidity)
	#Sensory.add_sensor(&"weather", func(): return Gnomon.weather)
	#Sensory.add_sensor(&"bonfire_wood", func(): return Gnomon.bonfire.wood)
	#Sensory.add_sensor(&"bonfire_heat", func(): return Gnomon.bonfire.heat)

static func item_count(collection: StringName, item: StringName):
	var r = Gnomon.current_instance[collection][item]
	if r is Realizable:
		return r.current_value()
	else:
		return r

static func fresh_instance():
	Gnomon.current_instance = Gnomon.new()
	#return Gnomon.current_instance

func UID():
	last_UID += 1
	return last_UID

func _default(save, val):
	if save == null:
		return val
	else:
		return save

func complete_event(ev: StringName) -> void:
	if ev not in game_events:
		game_events.push_back(ev)

func giddy_up(save = null):
	for e in Patron.collectables.entries:
		collectables[e.key_name] = _default(save, 0)
	for e in Patron.buildings.entries:
		buildings[e.key_name] = _default(save, Realizable.new(e.key_name))
		workforce[e.key_name] = _default(save, 0.0)
	for e in Patron.creatures.entries:
		creatures[e.key_name] = _default(save, Realizable.new(e.key_name))
		works_in = _default(save, {})
	for e in Patron.research.entries:
		research[e.key_name] = _default(save, {&"level": 0})
	visible_resources = _default(save, [&"amanita"])

func add_resource(collection: StringName, item: StringName, amount: float = 1.0, manual = null):
	if collection == &"collectables" and item not in visible_resources and Patron.item_count(collection, item) >= int(Patron.find_item(&"show", item, &"collectables")):
		GUI.Resources.show_resource(item)
	if manual == null:
		if self[collection][item] is Realizable:
			self[collection][item].auto_add(amount)
		else:
			self[collection][item] += amount
	else:
		self[collection][item].manual_add(manual)

func add_research(name: StringName, level: int = 1) -> void:
	research[name] = {&"level": level if level > 1 or name not in research else research[name].level + 1}

func realized(collection: StringName, item: StringName):
	return self[collection][item].manual

func is_working(c):
	if c.UID in works_in.keys():
		return true
	return false

func works_at(c):
	if c.UID in works_in.keys():
		return works_in[c.UID]
	return null

func building_slots_total(k: StringName) -> int:
	if k not in Gnomon.current_instance.buildings or Gnomon.current_instance.buildings[k].current_value() == 0:
		return 0
	else:
		return Gnomon.current_instance.buildings[k].current_value() * Patron.find_item(&"slots", k, &"buildings")

# amount of workers in a building type
func count_works_at(k: StringName) -> int:
	var r: int = 0
	for b in works_in:
		if works_in[b].kind == k:
			r += 1
	return r

# available workers for a building
func free_workers(k: StringName) -> int:
	var r: int = 0
	for ct in Patron.find_item(&"in", k, &"buildings"):
		r += free_creatures(ct)
	return r

# available workers of a type
func free_creatures(k: StringName) -> int:
	var r: int = 0
	for c in Gnomon.current_instance.creatures[k].manual:
		if c.UID not in works_in.keys():
			r += 1
	return r
