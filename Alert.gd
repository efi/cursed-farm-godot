extends PanelContainer

var t: float

func _gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and not event.pressed:
			GUI.beep.play()
			hide()
			GUI.Game.find_child(&"Log").show()

func _process(delta):
	t = t + delta
	modulate = lerp(Color.DIM_GRAY, Color.GRAY, ((sin(t * 6.0) / 4.0) + 1.0))
