extends Node2D

@export_range(0.0, 100.0, 0.1) var strength: float = 100.0
@export var score: int = 10

func _on_bumper_body_entered(body: Node2D) -> void:
	#print(body)
	if body is RigidBody2D:
		#print(body.mass + remap(strength, 0.0, 100.0, 0.0, 400.0))
		body.apply_central_impulse((body.global_position - self.global_position).normalized() * (body.mass + remap(strength, 0.0, 100.0, 0.0, 1000.0)))
		body.score.emit(score)
