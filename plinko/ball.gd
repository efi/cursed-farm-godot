extends RigidBody2D

signal score

func _on_body_entered(body: Node) -> void:
	var at = (body.global_position - global_position).normalized() * 16.0
	var speep = remap(linear_velocity.length(), 0.0, 2000.0, 1, 20)
	for _n in floori(speep):
		$sparks.emit_particle(transform.translated(at), Vector2.ZERO, Color.BLACK, Color.from_rgba8(0, 0, 0, randi_range(0, floori(remap(speep, 1.0, 20.0, 0, 255)))), GPUParticles2D.EmitFlags.EMIT_FLAG_POSITION + GPUParticles2D.EmitFlags.EMIT_FLAG_CUSTOM)
	if &"score" in body:
		score.emit(body.score)
