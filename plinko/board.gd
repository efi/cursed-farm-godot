class_name PachinkoBoard extends Node2D

var ball = preload("res://plinko/ball.tscn")
@export var tiles: Dictionary[PackedScene, int]
var _cam_target: Node2D
var _last_tile: PachinkoTile
var score: int
var _last_score: int

func _enter_tree():
	var b = ball.instantiate()
	add_child(b)
	b.score.connect(_add_score)
	b.position = get_viewport_rect().size / 2.0
	b.apply_central_impulse(Vector2(randf_range(-200.0, 200.0), randf_range(-200.0, 200.0)))
	%left_wall.position = b.position
	%left_wall.position.x -= 320.0
	%right_wall.position = b.position
	%right_wall.position.x += 320.0
	_cam_target = b
	%Camera2D.position = _cam_target.position

func _process(_delta):
	if is_node_ready():
		%Camera2D.position.y = _cam_target.position.y
		%left_wall.position.y = _cam_target.position.y
		%right_wall.position.y = _cam_target.position.y
		if _last_tile == null:
			var t = _get_tile().instantiate()
			add_child(t)
			t.position.y = _cam_target.position.y + 200
			t.position.x = (get_viewport_rect().size.x / 2.0) - 320
			_last_tile = t
		for c in get_children():
			if c is PachinkoTile:
				if c.position.y + 2000 < _cam_target.position.y:
					remove_child(c)
					c.queue_free()
		if _last_tile.position.y < _cam_target.position.y + 2000:
			var t = _get_tile().instantiate()
			add_child(t)
			t.position.y = _last_tile.position.y + _last_tile.tile_height
			t.position.x = (get_viewport_rect().size.x / 2.0) - 320
			_last_tile = t
		%Label.position.y = %Camera2D.get_screen_center_position().y - get_viewport_rect().size.y / 2.0 + 20.0
		%Label.text = &"Depth: %s\nScore: %s\nLS: %s" % [str(floori(_cam_target.position.y / 100.0)), score, _last_score]

func _add_score(n: int):
	score += n
	_last_score = n

func _get_tile():
	var total = 0
	for i in tiles.values():
		total += i
	var rand = randf() * total
	var acc = 0
	for k in tiles:
		if acc + tiles[k] < rand:
			acc += tiles[k]
		else:
			return k
