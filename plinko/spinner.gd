extends PinJoint2D

func _enter_tree():
	motor_target_velocity *= 1.0 if randf() > 0.5 else -1.0
