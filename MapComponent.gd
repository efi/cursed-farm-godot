extends Object
class_name MapComponent

var row: int
var col: int
var E
var N
var W
var S
var contents = []

static var prio: PackedStringArray = []

static func init_prio():
	for i in Patron.MapButtonPriority.entries:
		prio.push_back(i.key_name)

static func _static_init():
	MapComponent.init_prio.call_deferred()

func setindex(i: int, s: int):
	row = floor(i / s)
	col = posmod(i, s)
	E = GridLimit.east(i, s)
	N = GridLimit.north(i, s)
	W = GridLimit.west(i, s)
	S = GridLimit.south(i, s)

func add(item):
	#print(&"Add %s to %s:%s" % [item, col, row])
	if not item in contents:
		contents.push_back(item)

func remove(item):
	if item in contents:
		contents.erase(item)

func replace(item):
	contents = [item]

func display():
	if not contents.is_empty():
		if len(contents) == 1:
			return contents.front()
		for i in prio:
			if i in contents:
				return i
		return contents.back()
	return null

func can_build(v):
	return contents.all(func(i): return Patron.building_ts[v].can_build(i))
