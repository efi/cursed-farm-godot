class_name Spark extends Resource

@export var custom_script: Script

func run_script():
	var csi = custom_script.new()
	return csi.run()
