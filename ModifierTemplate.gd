class_name ModifierTemplate extends RefCounted

enum STACKING {UPDATE, TIME, POWER, IGNORE}
enum REMOVING {ALL, ONE, FUNC}

static var NOOP: Callable = func(): return null

static var _cats		:= {}
static var _stacks		:= {}
static var _removes		:= {}
static var _removal_fn	:= {}
static var _duration	:= {}
static var _actions		:= {}

static func add(n: StringName, mod: Dictionary) -> void:
	_cats[n] = (mod.category if &"category" in mod else []) as PackedStringArray
	if &"stacks" in mod:
		match mod.stacks:
			&"time":
				_stacks[n] = STACKING.TIME
			&"power":
				_stacks[n] = STACKING.POWER
			&"ignore":
				_stacks[n] = STACKING.IGNORE
			&"update":
				_stacks[n] = STACKING.UPDATE
	else:
		_stacks[n] = STACKING.UPDATE
	if &"removes" in mod:
		match mod.removes:
			&"one":
				_removes[n] = REMOVING.ONE
			&"func":
				assert(&"removal_fn" in mod, &"Modifier %s declares removal function, but does not provide one." % n)
				_removes[n] = REMOVING.FUNC
				_removal_fn[n] = mod.removal_fn
			&"all":
				_removes[n] = REMOVING.ALL
	else:
		_removes[n] = REMOVING.ALL
	_duration[n] = mod.time if &"duration" in mod else 0.0
	_actions[n] = {
		&"start"	: NOOP,
		&"add"		: NOOP,
		&"tick"		: NOOP,
		&"remove"	: NOOP,
		&"end"		: NOOP}
	if &"action_start" in mod:
		_actions[n].start = mod.action_start
	if &"action_add" in mod:
		_actions[n].add = mod.action_add
	if &"action_tick" in mod:
		_actions[n].tick = mod.action_tick
	if &"action_remove" in mod:
		_actions[n].remove = mod.action_remove
	if &"action_end" in mod:
		_actions[n].end = mod.action_end

static func use(n: StringName, visible = false) -> GameModifier:
	return GameModifier.new(n, visible)

static func categories(n: StringName) -> PackedStringArray:
	return _cats[n]

static func stacks(n: StringName) -> STACKING:
	return _stacks[n]

static func removes(n: StringName) -> REMOVING:
	return _removes[n]

static func duration(n: StringName) -> float:
	return _duration[n]

static func actions(n: StringName) -> Dictionary:
	return _actions[n]

static func removal_fn(n: StringName) -> Spark:
	return _removal_fn[n]
