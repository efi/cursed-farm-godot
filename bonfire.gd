extends MarginContainer
var pw: Price = Patron.price({&"collectables": {&"wood": 8}})
var pb: Price = Patron.price({&"collectables": {&"bone": 4}})

func add_wood():
	if Patron.can_pay(pw):
		Patron.pay_level(pw)
		Gnomon.current_instance.add_resource(&"bonfire", &"wood", 8)

func add_bones():
	if Patron.can_pay(pb):
		Patron.pay_level(pb)
		Gnomon.current_instance.add_resource(&"bonfire", &"bone", 4)
