class_name Research extends Resource

@export var name: StringName
@export var requirements: RequirementSet
@export var price: Price
@export var level: int = 0
@export var display: bool = false

func _init(n: StringName, req: RequirementSet, p: Price):
	name = n
	requirements = req
	price = p

func _to_string():
	return &"Research: %s %s %s" % [name, requirements, price]
