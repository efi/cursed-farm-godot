class_name AniCursor extends Node2D

var cursor_tex = preload("res://textures/cursor.png")
var cursor_atlas: Array[AtlasTexture]
var frame: float = 0
var frames: int = 0
const speed: float = 10.0
const size: Vector2i = Vector2i(32, 32)

func _init():
	frames = floori(float(cursor_tex.get_size().x) / size.x)
	for n in frames:
		var atlas = AtlasTexture.new()
		atlas.atlas = cursor_tex
		atlas.region = Rect2(n * size.x, 0.0, size.x, size.y)
		cursor_atlas.push_back(atlas)

func _process(_delta):
	if is_node_ready() and Input.get_current_cursor_shape() == Input.CURSOR_ARROW:
		frame = fposmod((Time.get_ticks_msec() / 1000.0) * speed, float(frames))
		Input.set_custom_mouse_cursor(cursor_atlas[frame], Input.CURSOR_ARROW)
