class_name PriceTemplate extends Resource

@export var price: Dictionary

func concretize():
	if price.is_empty():
		return Price.new()
	var p = price.duplicate(true)
	for c in p:
		for i in p[c]:
			p[c][i] = randi_range(p[c][i][0], p[c][i][1])
	return Patron.price(p)

func _to_string():
	var r: PackedStringArray = []
	for c in price.keys():
		for i in price[c]:
			r.push_back(&"%s.%s x(%s - %s)" % [c, i, price[c][i][0], price[c][i][1]])
	return &"PriceT[ %s ]" % &", ".join(r)
