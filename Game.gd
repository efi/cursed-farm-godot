extends MarginContainer

var Logline := preload("res://templates/log_line.tscn")
var logs := []
const _floating_window = preload("res://floating_window.tscn")

func _ready():
	if Session.state == Session.STARTING: GUI.Fadescreen.done.connect(finish_fadeout, CONNECT_ONE_SHOT)
	get_node(^"%Middle/TabContainer").get_tab_bar().tab_changed.connect(GUI.MapGUI.cancel_build_mode)
	get_node(^"%Menu button").pressed.connect(GUI.GameMenu.show)
	get_node(^"%DLC button").pressed.connect(GUI.DLCPopup.show)
	#Session.giddy_up()

func _unhandled_key_input(event):
	if event.is_action_released(&"ui_cancel"):
		if GUI.QuitPopup.visible:
			GUI.QuitPopup.hide()
		else:
			GUI.QuitPopup.show()

#func add(k_to, k_from, factor = 1.0, exponent = 1.0):
	#rates[k_to] += pow(Patron.find(k_from) * factor, exponent)
#
#func consume(k_to, k_from, factor = 1.0, exponent = 1.0):
#	rates[k_to] -= pow(Patron.find(k_from) * factor, exponent)

func update_tabs():
	for i in range(GUI.Tabs.get_tab_count()):
		GUI.Tabs.set_tab_hidden(i, should_hide_tab(i))

func should_hide_tab(i) -> bool:
	match i:
		0: return false #map
		1: return not Patron.is_event_complete(&"have_wood") #research
		2: return Patron.item_count(&"buildings", &"bonfire") == 0 #bonfire
		3: return not Patron.is_event_complete(&"gnome") #creatures
		4: return not Patron.is_research_level_complete(&"mining") #mines
		5: return false #pantheon
		6: return false #debug
		_: return true

#region Game log
func ingame_output(k):
	if len(logs) >= 1000:
		logs.pop_front()
	if GUI.Log.get_child_count() > 400:
		var rem: Node = GUI.Log.get_child(0)
		GUI.Log.remove_child(rem)
		rem.queue_free()
	if not logs.is_empty() and k in logs.back():
		logs.back()[k] += 1
		GUI.Log.get_child(GUI.Log.get_child_count() - 1).get_node(^"amount").text = str(logs.back()[k])
		for c in GUI.Log.get_child(GUI.Log.get_child_count() - 1).get_children(): c.show()
		return
	logs.push_back({k: 1})
	var L = Logline.instantiate()
	if Patron.collection_has_key(&"loglines", k):
		L.get_node(^"%text").text = Patron.find_item(&"text", k, &"loglines")
	else:
		L.get_node(^"%text").text = &"!!!log line %s not found!!!" % str(k)
	L.get_node(^"%amount").text = &"1"
	for c in L.get_children(): c.hide()
	L.get_node(^"%text").show()
	GUI.Log.add_child(L)

func ingame_alert(k):
	GUI.beep_high.play()
	get_node(^"%Alert").show()
	get_node(^"%Log").hide()
	GUI.Alert.text = Patron.find_item(&"text", k, &"loglines")
	ingame_output(k)
#endregion

func finish_fadeout():
	if Session.state == Session.STARTING:
		GUI.MainMenu.hide()
		GUI.Fadescreen.fade_in()
		Session.paused = false
		Session.state = Session.ACTIVE
		get_tree().paused = false

func _process(delta):
	if not Session.paused and Session.is_active():
		Session.tick(delta)
		update_tabs()


func _on_open_area_pressed() -> void:
	var l = %MainAreaSelector.location
	for c in %expandedworld.get_children():
		if c is FloatingWindow:
			if c.has_meta(&"location") and c.get_meta(&"location") == l:
				c.set_global_position(%expandedworld.get_viewport().get_camera_2d().get_screen_center_position() - c.get_rect().size * 0.5)
				c.show()
				return
	var w = _floating_window.instantiate()
	w.title = &"%s, %s >{%s, %s}" % [Patron.get_dimension_name(l.x), Patron.get_region_name(l), l.x, l.y]
	%expandedworld.add_child(w)
	w.set_global_position(%expandedworld.get_viewport().get_camera_2d().get_screen_center_position() - w.get_rect().size * 0.5)
	w.set_meta(&"location", l)
	w.show()
