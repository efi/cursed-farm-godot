extends RichTextLabel

func _process(_delta):
	if Session.state == Session.ACTIVE and visible:
		clear()
		for l in Session.debug:
			append_text(l)
			newline()
		#for res in Gnomon.collect:
			#append_text("%s: %s" % [Patron.display_names.plural[res], Patron.find(res + &"_rate")])
			#newline()
		pop_all()
		pass
