class_name Timing extends RefCounted

static var _timers: Dictionary[StringName, int]
static var _prev: Dictionary[StringName, int]

static func start(n: StringName) -> void:
	_timers[n] = Time.get_ticks_usec()
	_prev[n] = _timers[n]

static func check(n: StringName) -> void:
	assert(n in _timers)
	var usec: int = Time.get_ticks_usec()
	var t: int = usec - _timers[n]
	var d: int = usec - _prev[n]
	print(&"Time since %s started is %s µsec (%s Δ)" % [n, readable_num_str(t), readable_num_str(d)])
	_prev[n] = usec

static func readable_num_str(n) -> String:
	if n is int or n is float:
		return readable_num_str(str(n))
	if len(n) <= 3:
		return n
	else:
		return &"_".join([readable_num_str(n.left(-3)), n.right(3)])
