extends MarginContainer

#@onready var staff_list = find_child("staff_list")
@onready var Area_Selector := get_node(^"%AreaSelector")
@onready var workplaces := get_node(^"%workplaces")
@onready var staff := get_node(^"%staff_list")
@onready var candidates := get_node(^"%candidates_list")
@onready var none_av := get_node(^"%none_avail")
#const category = preload("res://templates/creature_category.tscn")
const building_row := preload("res://templates/building_row.tscn")
const row := preload("res://templates/creature_display.tscn")
const hire := preload("res://templates/creature_hire.tscn")
const creature_count := preload("res://templates/creature_count.tscn")
const display_max := 20
var spawn_max: int = 3

var workplace_nodes = []
var staff_nodes = []

func _ready():
	#for creature in Gnomon.creature:
		#hired[creature] = []
		#shown[creature] = false
		#var i = category.instantiate()
		#catbars[creature] = i
		#i.ref = creature
		#i.find_child(&"Label").text = Patron.display_names.plural[creature]
		#staff_list.add_child(i)
		#for n in display_max:
			#var r = row.instantiate()
			#r.find_child(&"Resource Icon").ref = creature
			#if Gnomon.DLC.hat:
				#r.find_child(&"DLC Label").show()
			#else:
				#r.find_child(&"DLC Label").hide()
			#staff_list.add_child(r)
	Timing.start(&"building rows")
	for b in Patron.buildings.entries:
		if &"workplace" in b.values.category:
			workplace_nodes.push_back(b.key_name)
			#var r = building_row.instantiate()
			#workplaces.add_child(r)
			#r.ref = b.name
			#r.hide()
	workplace_nodes.reverse()
	for c in Patron.creatures.entries:
		staff_nodes.push_back(c.key_name)
	staff_nodes.reverse()

	for n in display_max:
		var r = hire.instantiate()
		candidates.add_child(r)
	for r in candidates.get_children():
		r.hide()
	#refresh_staff_list()
	Timing.check(&"building rows")

func _process(_delta):
	if visible:
		if not workplace_nodes.is_empty():
			var r = building_row.instantiate()
			var b = workplace_nodes.pop_back()
			r.ref = b
			r.hide()
			workplaces.add_child(r)
		if not staff_nodes.is_empty():
			var r = creature_count.instantiate()
			var c = staff_nodes.pop_back()
			r.ref = c
			r.hide()
			staff.add_child(r)

func can_spawn_now():
	var r = []
	for c in Patron.creatures.entries:
		if Patron.is_research_level_complete(c.key_name):
			r.push_back(CreatureTemplate.new(c.key_name))
	return r

func reroll_candidates():
	var can_spawn = can_spawn_now()
	if not can_spawn.is_empty():
		for i in spawn_max:
			new_candidate_at(i, can_spawn)

func new_candidate_at(i, can_spawn):
	var spawn = can_spawn.pick_random()
	var r = candidates.get_child(i)
	var c = Creature.new(spawn)
	r.icon.icon = c.kind
	if Gnomon.current_instance.DLC.hat:
		r.L_dlc.show()
	else:
		r.L_dlc.hide()
	r.L_type.text = Patron.find_item(&"singular", c.kind, &"display_names")
	r.L_age.text = str(c.age)
	r.L_dlc.text = c.hat
	#print(c.price)
	r.price.price = c.price
	r.hire.creature = c
	r.L_name.text = c.name
	r.show()

func partial_refresh_candidate_list():
	var can_spawn = can_spawn_now()
	if not can_spawn.is_empty():
		for i in spawn_max:
			if candidates.get_child(i).visible:
				continue
			else:
				new_candidate_at(i, can_spawn)


func refresh_building_list():
	none_av.show()
	for r in workplaces.get_children():
		if r == none_av: continue
		if Gnomon.current_instance.buildings[r.ref].current_value() > 0:
			none_av.hide()
			r.show()
#func refresh_staff_list():
	#for c in Gnomon.creature:
		#var hired_c = hired[c].duplicate()
		#for i in display_max:
			#var r = catbars[c].get_parent().get_child(catbars[c].get_index() + i + 1)
			#if shown[c] and not hired_c.is_empty():
				#var h = hired_c.pop_at(floori(randf() * len(hired_c)))
				#r.find_child(&"Type").text = Patron.display_names.singular[c]
				#r.find_child(&"DLC Label").text = h.hat
				#r.find_child(&"Name").text = h.name
				#r.find_child(&"Work").text = Patron.display_names.singular[h.works_at]
				#r.show()
			#else:
				#r.hide()

func show_creature(ref):
	for r in staff.get_children():
		if ref == r.ref:
			r.show()
