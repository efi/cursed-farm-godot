=== CREDITS ===

== Design, Direction and Programming ==
* Efi

== Additional Design ==
* Rai
* Naomi

== Interface Artwork ==
* Sachi

== Font ==
* Fool (CC BY 4.0) https://arcade.itch.io/fool

== Incidental Advisors ==
* Ben
* Alexandria
* Aeva

== Special Thanks ==
* Ramsey Nasser
* Kitty Horror Show
* Alexandra
* Reki
* Emily
* Jazzmin
* Elleanor
* Jennifer
* Morg
* Mel
* Tuula
* Cinnamon
* Sophie Jane
* Nikita Lisitsa
* Del

* And all the girls who supported me on the way...


Shoutouts to SimpleFlips
