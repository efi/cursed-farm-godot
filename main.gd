extends PanelContainer

const game := preload("res://game.tscn")

func quit():
	GUI.beep.play()
	GUI.Fadescreen.fade_out(1.0)
	GUI.Fadescreen.done.connect(Session.quit, CONNECT_ONE_SHOT)

func start():
	if Session.state == Session.MAIN_MENU:
		GUI.beep.play()
		GUI.Fadescreen.fade_out()
		await GUI.Fadescreen.done
		Timing.start(&"game")
		print(&"Starting game...")
		Session.state = Session.STARTING
		Patron.giddy_up()
		var G = game.instantiate()
		GUI.fetch(G)
		Timing.check(&"game")
		Gnomon.current_instance.visible_resources = [&"amanita"]
		Gnomon.current_instance.dimension_name = {0: &"Earthly Plane"}
		Gnomon.current_instance.region_name = {Vector2i.ZERO: &"Forest"}
		Gnomon.current_instance.map[Vector2i.ZERO] =\
			Map.new(Vector2i.ZERO, MapGenerator.templates.forest)
		#GUI.MainMenu.hide()
		GUI.Main.add_child(G)
		GUI.Main.move_child(G, 0)
		GUI.MapGUI.show_map(Vector2i.ZERO)
		var slot = GUI.MapGUI.find_mushroom_slot(GUI.MapGUI.current_display)
		GUI.MapGUI.update_slot(GUI.MapGUI.current_display, slot, &"gnome_house")
		Gnomon.current_instance.buildings.gnome_house.manual_add(Building.new(Patron.building_ts.gnome_house, Vector2i.ZERO, slot))
		#Session.paused = false
		Timing.check(&"game")
		#await GUI.Fadescreen.done

func _ready():
	get_tree().paused = true
	GUI.main(self)
	GUI.Fadescreen.fade_in(0.75, true)
	%start.grab_focus.call_deferred()
	add_child.call_deferred(AniCursor.new())
