extends MarginContainer

@onready var _portrait = $Portrait
@onready var _text = find_child(&"Text")

var character:
	set(v):
		_portrait.texture = Patron.sprites[v] if v in Patron.sprites else null
var text:
	set(v):
		_text.text = v

func show_dialogue(new_text, new_character := &""):
	text = new_text
	character = new_character
	show()
