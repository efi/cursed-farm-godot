Witches are the most complext idea in the game so far.
They are all lesbiansm but that doesn't mean they can't be varied.
Useful stats that change gameplay:
- gender: butch, femme, twink, other adjectives, determine the "weapon type" they can equip; for example, a butch may be able to wield a sword, where a femme would have a staff and a twink a wand, etc; this determines their **Main Action** which is something like "Defend", "Alter", or "Talk", but never "Attack", "Damage", "Harm"...
- sign: astrological compatibility; witches are paired with other witches and this sign determines how strong and which bonuses the team gets.
- health: one of "healthy", "unhealthy", "dying", "dead"; determines the actions it can perform, increasingly limited. Dead witches can only take the **Resurrect** attempt action.
- infatuation: a scale that improves the performance based on who the other team member is; a specific other is chosen sometimes when a teamup with a certain compatibility is made. unidirectional. may have multiple.
- home: the geographical location of this witch's home, which determines calculations for commute times and where summoned entities appear.
- Luck: can be "good" or "bad" and a number; determines how strongly the random-chance-based actions are affected. good/bad luck is not a bonus/malus, but it determines the flavor of the actions affected; good luck brings nice things, bad luck brings demonic things. both useful in their ways.

Other stats:
- age: cosmetic, lived time
- blood type: cosmetic, one of "A", "B", "AB", "O", or things like "acid", "lava", "sugar", "demon blood", etc.
- deity: if a witch favor a particular deity, may or may not affect gameplay, idk.
- favorite color: cosmetic, may affect the rendered sprite.
- style: cosmetic, may change the sprite model.

Equipment:
- Weapon: changes the **Main Action** as described in "gender".
- Amulets: an array of trinkets that have different effects.
- Broom: determines the commute speed and can also have other effects.

