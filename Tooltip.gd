extends PanelContainer
@onready var label = find_child("Label")
@onready var ricon = find_child("Resource Icon")
@onready var price = find_child("price_tag")

func update(text: StringName, icon: StringName = &"", p: Price = null):
	#print(text)
	var disp = Patron.find_item(&"singular", text, &"display_names")
	if disp == null or disp.is_empty():
		disp = &"#ERROR"
	label.text = disp
	ricon.show()
	if icon.is_empty():
		ricon.hide()
	else:
		ricon.icon = icon
	if p == null:
		price.hide()
	else:
		price.price = p
		price.show()
	reset_size()

func _notification(what: int) -> void:
	if what == NOTIFICATION_MOUSE_ENTER:
		hide.call_deferred()
