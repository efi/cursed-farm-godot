class_name PriceSplitter extends Object

static func split(s: String) -> Dictionary:
	if s.is_empty():
		return {}
	var r = {}
	for sp in s.split(&","):
		var kv = sp.split(&" ")
		var ks = kv[0].split(&".")
		if len(ks) != 2: push_error(&"Price address %s is invalid" % kv)
		if ks[0] not in r: r[ks[0]] = {}
		r[ks[0]][ks[1]] = kv[1].to_int()
	return r

static func splitTemplate(s: String) -> Dictionary:
	if s.is_empty():
		return {}
	var r = {}
	for sp in s.split(&","):
		var kv = sp.split(&" ")
		var ks = kv[0].split(&".")
		if len(ks) != 2: push_error(&"Price address %s is invalid" % kv)
		#printt(ks, kv)
		var v = (kv[1].split(&"-") as Array)
		if len(v) > 2: push_error(&"Invalid range %s in price template." % v)
		v[0] = v[0].to_int()
		if len(v) == 1:
			v.push_back(v[0])
		else:
			v[1] = v[1].to_int()
		if ks[0] not in r: r[ks[0]] = {}
		r[ks[0]][ks[1]] = (v as PackedInt32Array)
	return r
