extends RefCounted

func split(line: String) -> Array:
	var splits = line.split(&" ", true, 1) as Array
	#split[0] unchanged
	var reqs: Array[Requirement] = []
	if len(splits) == 1: return splits
	var rs = RequirementSet.new()
	for r1 in splits[1].split(&";"):
		var r2 = r1.split(&" or ")
		for r3 in r2:
			var q = Requirement.new()
			q.condition = r3
			rs.reqs.push_back(q)
	rs.parse_all()
	splits[1] = rs
	return splits
