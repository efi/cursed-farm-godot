extends RefCounted

func split(line: String) -> Array:
	var splits: Array = line.split(&" ")
	splits[2] = splits[2].to_int()
	return splits
