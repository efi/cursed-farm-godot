extends RefCounted

func split(line: String) -> Array:
	var splits: Array
	splits = line.split(&";") as Array
	var length = len(splits)
	#name,conditions,price
	if length == 1:
		return [splits[0], RequirementSet.placeholder(), Price.new()]
	if length == 2:
		return [splits[0], RequirementSet.placeholder(), Patron.price(splits[1])]
	if length > 2:
		var p = splits.pop_back()
		var rs = splits.slice(1)
		splits = [splits[0], null, null]
		splits[1] = RequirementSet.new()
		var reqs: Array[Requirement] = []
		reqs.resize(len(rs))
		for ri in len(rs):
			reqs[ri] = Requirement.new()
			reqs[ri].condition = rs[ri]
		splits[1].reqs = reqs
		splits[1].parse_all()
		splits[2] = Patron.price(PriceSplitter.split(p))
		#print_debug(splits[1])
		return splits
	return []
