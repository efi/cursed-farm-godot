extends RefCounted

func split(line: String) -> Array:
	var splits: Array
	splits = line.split(&";") as Array
	splits.resize(3)
	#columns,age,price
	#splits[0] unchanged
	var age = [20, 30]
	if &"-" in splits[1]:
		var a = splits[1].split(&"-")
		age = Vector2i(int(a[0]), int(a[1]))
	else:
		age = Vector2i(int(splits[1]), int(splits[1]))
	splits[1] = age
	splits[2] = PriceTemplate.new() if splits[2] == null or splits[2].is_empty() else Patron.price_template(PriceSplitter.splitTemplate(splits[2]))
	return splits
