extends RefCounted

func split(line: String) -> PackedStringArray:
	return line.split(&",")
