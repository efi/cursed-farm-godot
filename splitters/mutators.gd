extends RefCounted

func split(line: String) -> Array:
	var splits: Array
	splits = line.split(&";") as Array
	#columns,requirement,input,output,multiplier,exponent
	if len(splits) != 6:
		push_error(&"Error in Mutator!!! %s" % line)
	var rs = RequirementSet.new()
	var reqs = splits[1].split(&" and ")
	var reqos : Array[Requirement] = []
	reqos.resize(len(reqs))
	for ri in len(reqs):
		reqos[ri] = Requirement.new()
		reqos[ri].condition = reqs[ri]
	rs.reqs = reqos
	rs.parse_all()
	print(rs)
	splits[1] = rs
	splits[2] = splits[2]
	splits[3] = splits[3]
	splits[4] = float(splits[4])
	splits[5] = float(splits[5])
	return splits
