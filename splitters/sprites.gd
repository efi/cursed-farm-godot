extends RefCounted

func split(line: String) -> Array:
	var splits = line.split(&",") as Array
	splits[1] = load("res://textures/%s.png" % splits[1])
	splits[2] = load("res://textures/%s.png.atlas" % splits[2])
	return splits
