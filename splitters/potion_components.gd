extends RefCounted

func split(line) -> Array:
	var splits = line.split(&";") as Array
	#split[0] unchanged
	var patterns: Array = []
	for i in len(splits):
		if i == 0: continue
		var pattern = splits[i].split(",") as Array
		for pat_line in len(pattern):
			pattern[pat_line] = pattern[pat_line].split(" ")
		#print(pattern)
		patterns.push_back(pattern)
	splits[1] = patterns
	return splits
