extends RefCounted

func split(line) -> PackedStringArray:
	return line.split(&";")
