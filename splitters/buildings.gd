extends RefCounted

func split(line: String) -> Array:
	var splits: Array
	splits = line.split(&";") as Array
	splits.resize(7)
	#columns,category,on,in,slots,price,time
	#splits[0] unchanged
	splits[1] = []				if splits[1] == null or splits[1].is_empty() else splits[1].split(&" ")
	splits[2] = []				if splits[2] == null or splits[2].is_empty() else splits[2].split(&" ")
	splits[3] = []				if splits[3] == null or splits[3].is_empty() else splits[3].split(&" ")
	splits[4] = 0				if splits[4] == null or splits[4].is_empty() else splits[4].to_int()
	splits[5] = Price.new()		if splits[5] == null or splits[5].is_empty() else Patron.price(PriceSplitter.split(splits[5]))
	splits[6] = 0.0				if splits[6] == null or splits[6].is_empty() else splits[6].to_float()
	return splits
