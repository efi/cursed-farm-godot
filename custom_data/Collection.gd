@tool
class_name Collection extends Resource

@export var columns: PackedStringArray
@export var entries: Array[Entry]
