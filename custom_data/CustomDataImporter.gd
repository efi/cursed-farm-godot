@tool
class_name CustomDataImporter extends EditorPlugin

var collections: EditorImportPlugin = load("res://custom_data/ImportCollection.gd").new()
var atlases: EditorImportPlugin = load("res://custom_data/ImportAtlases.gd").new()
var cdi: EditorInspectorPlugin = load("res://custom_data/CustomDataInspector.gd").new() #inspector
var bottom: Control = load("res://custom_data/BottomPanel.tscn").instantiate() #gui scene

func _enter_tree():
	add_import_plugin(collections)
	add_import_plugin(atlases)
	add_inspector_plugin(cdi)
	add_control_to_bottom_panel(bottom, "Collections")

func _exit_tree():
	remove_import_plugin(collections)
	remove_import_plugin(atlases)
	remove_inspector_plugin(cdi)
	remove_control_from_bottom_panel(bottom)
	bottom.queue_free()

static func _static_init():
	if Engine.is_editor_hint():
		EditorInterface.get_editor_main_screen().add_child(CustomDataImporter.new())
