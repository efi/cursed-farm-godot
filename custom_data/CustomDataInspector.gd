extends EditorInspectorPlugin

var cdpe = load("res://custom_data/CustomDataProperty.gd") #property editor

func _can_handle(object: Object) -> bool:
	return true

func _parse_category(object: Object, category: String) -> void:
	#printt("Category:", object, category)
	if category == "Collection.gd":
		add_custom_control(Button.new())

#func _parse_group(object: Object, group: String) -> void:
#	printt("Group:", object, group)

func _parse_property(object: Object, type: Variant.Type, name: String, hint_type: PropertyHint, hint_string: String, usage_flags: int, wide: bool) -> bool:
	if type == TYPE_OBJECT and hint_string == &"Collection":
		add_property_editor(name, cdpe.new())
		return true
	return false
