class_name ImportDataCollection extends EditorImportPlugin

func _can_import_threaded() -> bool:
	return true

func _get_importer_name():
	return "importer.collection"

func _get_visible_name():
	return "Collection"

func _get_recognized_extensions():
	return ["data"]

func _get_save_extension():
	return "tres"

func _get_resource_type():
	return "Collection"

func _get_preset_count():
	return 1

func _get_priority():
	return 1.0

func _get_import_order():
	return 0

func _get_preset_name(preset_index):
	match preset_index:
		_:
			return "Collection"

var default_splitter = load("res://custom_data/DefaultSplitter.gd")

func _get_import_options(path, preset_index):
	return [{&"name": &"splitter",
			 &"default_value": default_splitter,
			 &"property_hint": PROPERTY_HINT_RESOURCE_TYPE,
			 &"hint_string": &"Script",}]

func _get_option_visibility(path: String, option_name: StringName, options: Dictionary) -> bool:
	return true

func _import(source_file, save_path, options, platform_variants, gen_files):
	var err
	if not FileAccess.file_exists(source_file):
		return ERR_FILE_BAD_PATH
	var file = FileAccess.open(source_file, FileAccess.READ)
	if file == null:
		return ERR_CANT_OPEN
	var c = Collection.new()
	var s = options.splitter.new()
	while file.get_position() < file.get_length():
		var line = file.get_line()
		if not line.is_empty():
			if line.begins_with(&"#columns"):
				var splits = line.split(&",", false)
				if len(splits) > 1:
					for split in splits.slice(1):
						c.columns.push_back(split)
			if line.begins_with(&"#"):
				continue
			var res = Entry.new()
			var splits = s.split(line)
			res.key_name = splits[0]
			var l = len(splits)
			if l > 1:
				splits = splits.slice(1)
				for n in len(c.columns):
					if n < l:
						res.values[c.columns[n]] = splits[n]
				#print_debug(res.values)
			c.entries.push_back(res)
			#var filename = &"%s.%s.%s" % [save_path, res.key_name, _get_save_extension()]
			#err = ResourceSaver.save(res, filename)
			#if err != OK:
				#return err
			#gen_files.push_back(filename)
	err = ResourceSaver.save(c, &"%s.%s" % [save_path, _get_save_extension()])
	if err != OK:
		return err
	return OK
