@tool
class_name TextureAtlas extends Resource

@export var main_texture: Texture2D
@export var sub_textures: Dictionary
@export var size: Vector2i
