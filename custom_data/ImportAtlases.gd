class_name ImportAtlases extends EditorImportPlugin

func _can_import_threaded() -> bool:
	return true

func _get_importer_name():
	return "importer.atlases"

func _get_visible_name():
	return "Atlas"

func _get_recognized_extensions():
	return ["atlas"]

func _get_save_extension():
	return "tres"

func _get_resource_type():
	return "AtlasTexture"

func _get_preset_count():
	return 1

func _get_priority():
	return 1.0

func _get_import_order():
	return 0

func _get_preset_name(preset_index):
	match preset_index:
		_:
			return "Atlas"

func _get_import_options(path, preset_index):
	return []

func _get_option_visibility(path: String, option_name: StringName, options: Dictionary) -> bool:
	return true

func _import(source_file, save_path, options, platform_variants, gen_files):
	var err
	if not FileAccess.file_exists(source_file):
		return ERR_FILE_BAD_PATH
	var file = FileAccess.open(source_file, FileAccess.READ)
	if file == null:
		return ERR_CANT_OPEN
	var t = load(source_file.trim_suffix(&".atlas"))
	var TA = TextureAtlas.new()
	TA.main_texture = t
	var size_s = file.get_line().split(&" ", false)
	var size = Vector2i(size_s[0].to_int(), size_s[1].to_int())
	TA.size = size
	var line_n = 0
	while file.get_position() < file.get_length():
		var line = file.get_line()
		if not line.is_empty():
			if line.begins_with(&"#"):
				continue
			var splits = line.split(&" ", false)
			for i in len(splits):
				if splits[i] != &"_":
					var a = AtlasTexture.new()
					a.atlas = t
					a.region = Rect2(size.x * i, size.y * line_n, size.x, size.y)
					var filename = &"%s:%s.%s" % [save_path, splits[i], _get_save_extension()]
					err = ResourceSaver.save(a, filename)
					if err != OK:
						return err
					gen_files.push_back(filename)
					TA.sub_textures[splits[i]] = a
			line_n += 1
	err = ResourceSaver.save(TA, &"%s.%s" % [save_path, _get_save_extension()])
	if err != OK:
		return err
	return OK
