extends EditorProperty

var c: Collection
var mb: MenuButton
var value: Entry

func _init() -> void:
	mb = MenuButton.new()
	add_child(mb)
	mb.get_popup().id_pressed.connect(_select)
	mb.text = &"Error"
	for id in len(c.entries):
		mb.get_popup().add_radio_check_item(c.entries[id].key_name, id)
	refresh_control()

func _select(id):
	#print(&"Selected %s" % id)
	var new_value = c.entries[id]
	#if value != null and new_value != null:
	#	print(&"Value: %s\nNew value: %s" % [value.key_name, new_value.key_name])
	if (new_value == value):
		return
	value = new_value
	get_edited_object().set(get_edited_property(), value)
	refresh_control()
	emit_changed(get_edited_property(), value)

func _update_property():
	value = get_edited_object()[get_edited_property()]
	refresh_control()

func refresh_control():
	mb.text = &"None" if value == null else value.key_name
	for index in mb.item_count:
		#print(&"Set %s to %s when matching %s and %s" % [str(n), str(value.key_name == cs.collectables[mb.get_popup().get_item_id(n)].key_name), value.key_name, cs.collectables[mb.get_popup().get_item_id(n)].key_name])
		mb.get_popup().set_item_checked(index, false if value == null else value.key_name == c.entries[mb.get_popup().get_item_id(index)].key_name)
