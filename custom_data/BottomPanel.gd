@tool
extends Control

var data_files := {}
var _nth
var _splitter
func _enter_tree():
	_nth = 0
	%Collection.clear()
	scan_dir(&"res://")
	_nth = null

func scan_dir(path: String) -> void:
	var files := DirAccess.get_files_at(path)
	for f in files:
		if f.ends_with(&".data"):
			data_files[_nth] = {&"path": path.path_join(f), &"name": f.get_basename()}
			%Collection.add_item(data_files[_nth].name, _nth)
			_nth += 1
	var dirs := DirAccess.get_directories_at(path)
	for d in dirs:
		if not d.ends_with(&"addons") and not d.ends_with(&"script_templates"):
			scan_dir(d)

func _on_collection_item_selected(index):
	for n in %Grid.get_children():
		%Grid.remove_child(n)
	%Grid.columns = 1
	var splitter = &""
	if FileAccess.file_exists(data_files[index].path + &".import"):
		var import = FileAccess.get_file_as_string(data_files[index].path + &".import")
		if &"splitter" in import:
			for l in import.split(&"\n"):
				if l.begins_with(&"splitter"):
					splitter = l.trim_prefix(&"splitter=Resource(\"").trim_suffix(&"\")")
	if splitter.is_empty():
		_splitter = load("res://custom_data/DefaultSplitter.gd").new()
	else:
		_splitter = load(splitter).new()
	for l in FileAccess.get_file_as_string(data_files[index].path).split(&"\n"):
		if l.is_empty(): continue
		if l.begins_with(&"#columns"):
			%Grid.columns = len(l.split(&","))
		if l.begins_with(&"#"): continue
		for e in _splitter.split(l):
			var I
			if e is Resource:
				I = Button.new()
				I.text = str(e)
				I.pressed.connect(EditorInterface.edit_resource.bind(e))
			else:
				I = Label.new()
				I.text = str(e)
			%Grid.add_child(I)
