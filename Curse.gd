class_name Curse extends Resource

var _requirements: RequirementSet
var _effect: Spark

func _init(rs: RequirementSet, ef: Spark):
	_effect = ef
	_requirements = rs

func check():
	if _requirements.check():
		_effect.run()
