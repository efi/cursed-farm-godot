class_name Map extends Object

var _dimension: int
var _region: int
@export var address: Vector2i
@export var size: int
var tiles: Array[MapComponent] = []
var length:
	get:
		return len(tiles)

func set_address(v: Vector2i):
	address = v
	_dimension = v[0]
	_region = v[1]

func set_dimension(i: int):
	set_address(Vector2i(i, _region))

func set_region(i: int):
	set_address(Vector2i(_dimension, i))

func _init(a: Vector2i, t: MapTemplate, s: int = 13):
	set_address(a)
	size = s
	tiles = MapGenerator.new_terrain(t, size)

func tile_get(i: int) -> MapComponent:
	assert(i >= 0 and i < len(tiles))
	return tiles[i]

func tile_set(i: int, v: StringName) -> void:
	assert(i >= 0 and i < len(tiles))
	tiles[i].replace(v)

func tile_add(i: int, v: StringName) -> void:
	assert(i >= 0 and i < len(tiles))
	tiles[i].add(v)

func tile_remove(i: int, v: StringName) -> void:
	assert(i >= 0 and i < len(tiles))
	tiles[i].remove(v)

func any_neighbor(c: MapComponent, value) -> bool:
	return 	(c.E != -1 and value in tiles[c.E].contents) or \
			(c.N != -1 and value in tiles[c.N].contents) or \
			(c.W != -1 and value in tiles[c.W].contents) or \
			(c.S != -1 and value in tiles[c.S].contents)

func exactly(c: MapComponent, value) -> bool:
	return c.contents == [value]
