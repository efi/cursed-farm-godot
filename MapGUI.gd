extends MarginContainer
var current_display: Vector2i = Vector2i(0, 0)
@onready var Map_display = get_node(^"%MapGrid")
@onready var Area_Selector = get_node(^"%AreaSelector")
@onready var Map_button = preload("res://templates/map_button.tscn")
@onready var build = get_node(^"%Build")
@onready var context = get_node(^"%Context")
@onready var CT = preload("res://templates/construction_timer.tscn")
@onready var pop = GUI.Game.get_node(^"sounds/pop")
const MAPSIZE := 13

func _ready():
	Timing.start(&"map ready")  
	Map_display.columns = MAPSIZE
	for n in (MAPSIZE * MAPSIZE):
		var b = Map_button.instantiate()
		b.custom_minimum_size = Vector2(32, 32)
		b.index = n
		b.mouse_filter = MOUSE_FILTER_PASS
		Map_display.add_child(b)
		link_tooltip.call_deferred(b)
	for b in Map_display.get_children():
		b.set_focus_neighbor(SIDE_RIGHT,	b.get_path_to(Map_display.get_child(b.index + 1))			if posmod(b.index + 1, MAPSIZE)	> 0									else NodePath(&""))
		b.set_focus_neighbor(SIDE_TOP,		b.get_path_to(Map_display.get_child(b.index - MAPSIZE))		if b.index - MAPSIZE			> 0									else NodePath(&""))
		b.set_focus_neighbor(SIDE_LEFT,		b.get_path_to(Map_display.get_child(b.index - 1))			if posmod(b.index - 1, MAPSIZE)	< MAPSIZE - 1						else NodePath(&""))
		b.set_focus_neighbor(SIDE_BOTTOM,	b.get_path_to(Map_display.get_child(b.index + MAPSIZE))		if b.index + MAPSIZE			< Map_display.get_child_count()		else NodePath(&""))
	Map_display.get_parent().mouse_exited.connect(context.update.bind(-1))
	Timing.check(&"map ready")

func link_tooltip(b: Button) -> void:
	b.mouse_exited.connect(GUI.Tooltip.hide)

func cancel_build_mode(__) -> void:
	build.disable_build_mode()

# !!! current_map here refers to the currently being processed map, not the currently being displayed map!
func find_slot_filter(current_map: Vector2i, filt: Callable) -> int:
	var i = range(Gnomon.current_instance.map[current_map].length - 1)
	i = i.filter(func(n: int): return filt.call(Gnomon.current_instance.map[current_map].tile_get(n)))
	if not i.is_empty():
		return i.pick_random()
	#print(&"No slot found")
	return -1

func find_mushroom_slot(current_map):
	return find_slot_filter(current_map,
		func(c: MapComponent): return c.contents == [MapTemplate.FOREST])

func find_belladona_slot(current_map: Vector2i) -> int:
	var m: Map = Gnomon.current_instance.map[current_map]
	return find_slot_filter(current_map,
		func(c: MapComponent):
			return 	m.exactly(c, MapTemplate.PLAINS) and \
					m.any_neighbor(c, MapTemplate.FOREST))

func update_slot(current_map: Vector2i, n: int, thing: StringName) -> void:
	if n < 0: return
	Gnomon.current_instance.map[current_map].tile_add(n, thing)
	Map_display.get_child(n).update()

func slot_remove(current_map: Vector2i, n: int, thing: StringName) -> void:
	if n < 0: return
	Gnomon.current_instance.map[current_map].tile_remove(n, thing)
	Map_display.get_child(n).update()

func slot_replace(current_map: Vector2i, n: int, thing: StringName) -> void:
	if n < 0: return
	Gnomon.current_instance.map[current_map].tile_replace(n, thing)
	Map_display.get_child(n).update()

func spawn_mushroom() -> void:
	#print(&"try mushroom")
	var p = 2
	var w = Sensory.sense(&"weather")
	if w == 3 or w == 4:
		p = 5
	for current_map in Gnomon.current_instance.map:
		if randf() < 0.7:
			for __ in randi_range(1, p):
				#print(&"spawn mushroom")
				update_slot(current_map, find_mushroom_slot(current_map), &"amanita")

func spawn_wood() -> void:
	for current_map in Gnomon.current_instance.map:
		if randf() < 0.8:
			for __ in randi_range(0, clamp(Sensory.sense(&"weather"), 1, 5)):
				update_slot(current_map, find_mushroom_slot(current_map), &"wood")

func spawn_belladona() -> void:
	for current_map in Gnomon.current_instance.map:
		for __ in randf_range(0, 2):
			update_slot(current_map, find_belladona_slot(current_map), &"belladona")

func action(ref: StringName, id: int) -> void:
	var _mod := 0.0
	var button_rect = Map_display.get_child(id).get_global_rect()
	match ref:
		&"amanita":
			GUI.Main.get_node(^"%Amanita Particle").emit_particle(Transform2D(0.0, button_rect.position + button_rect.size / 2), Vector2(), Color.BLACK, Color.BLACK, GPUParticles2D.EMIT_FLAG_POSITION)
			slot_remove(current_display, id, &"amanita")
			GUI.Game.ingame_output(&"amanita")
			Gnomon.current_instance.add_resource(&"collectables", &"amanita", floor(randf() * 10 + 1))
			pop.play()
		&"wood":
			GUI.Main.get_node(^"%Wood Particle").emit_particle(Transform2D(0.0, button_rect.position + button_rect.size / 2), Vector2(), Color.BLACK, Color.BLACK, GPUParticles2D.EMIT_FLAG_POSITION)
			slot_remove(current_display, id, &"wood")
			GUI.Game.ingame_output(&"wood")
			Gnomon.current_instance.add_resource(&"collectables", &"wood", 30)
			pop.play()
		&"belladona":
			GUI.Main.get_node(^"%Belladona Particle").emit_particle(Transform2D(0.0, button_rect.position + button_rect.size / 2), Vector2(), Color.BLACK, Color.BLACK, GPUParticles2D.EMIT_FLAG_POSITION)
			slot_remove(current_display, id, &"belladona")
			GUI.Game.ingame_output(&"belladona")
			Gnomon.current_instance.add_resource(&"collectables", &"belladona", floor(randf() * 10 + 1))
			pop.play()
		_:
			if build.build_mode:
				var T: BuildingTemplate = Patron.building_ts[ref]
				if Gnomon.current_instance.map[GUI.MapGUI.current_display].tile_get(id).can_build(ref) and T.can_pay():
					GUI.Game.ingame_output(T.name)
					T.pay()
					update_slot(current_display, id, ref)
					Map_display.get_child(id).forbidden_outline(ref)
					var CTimer = CT.instantiate()
					Map_display.get_child(id).add_child(CTimer)
					CTimer.set_time(T.construction_time)
					CTimer.end = func():
						Gnomon.current_instance.buildings[ref].manual_add(Building.new(T, current_display, id))
						GUI.Creatures.refresh_building_list()
				else:
					GUI.beep_error.play()

func show_map(m: Vector2i) -> void:
	current_display = m
	for b in Map_display.get_children():
		b.update()

func switch_map() -> void:
	show_map(Area_Selector.reg.selected)
