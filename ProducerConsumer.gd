class_name ProducerConsumer extends RefCounted

var condition: Variant:
	set(v):
		if v == null:
			condition = func() -> bool: return source.call() > 0
		else:
			assert(v is Callable)
			condition = v
		return true
var modifies: StringName:
	set(v):
		assert(v in Gnomon.current_instance.collectables or v in Gnomon.current_instance.creatures or v in Gnomon.current_instance.buildings)
		modifies = v
		return true
var source: Variant:
	set(v):
		assert(v is StringName or v is Callable)
		if v is StringName:
			if Patron.collection_has_key(&"buildings", v):
				source = func() -> float: return Sensory.sense(&"active_workers_" + v) / float(Patron.find_item(&"base_slots", v, &"buildings"))
			else:
				#source = func() -> float: return Patron.find_item(&"", v, &"collectables")
				push_error(&"???")
		else:
			assert(v is Callable)
			source = v
		return true
var multiplier: float
var exponent: float

func _init(c, m, s, f = 1.0, e = 1.0):
	condition = c
	modifies = m
	source = s
	multiplier = f
	exponent = e

func get_rate() -> float:
	if condition.call():
		return pow(source.call() * multiplier, exponent)
	return 0.0

