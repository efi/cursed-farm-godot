extends Spark

func run():
	Session.add_once_timer(&"gnome_event", randf_range(5.0, 10.0), func():
		GUI.Game.ingame_alert(&"belladona_event")
		GUI.Resources.show_resource(&"belladona"))
