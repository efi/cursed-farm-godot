extends Spark

func run():
	Session.add_once_timer(&"gnome_event", randf_range(5.0, 10.0), func():
		Gnomon.current_instance.add_research(&"gnome")
		GUI.Creatures.show_creature(&"gnome")
		GUI.Game.ingame_alert(&"gnome_event"))
