class_name CreatureTemplate extends RefCounted

var kind: StringName
var names: Collection
var age: Vector2i
var prices: PriceTemplate

func _init(k: StringName):
	kind = k
	names = load(&"res://data/%s_names.data" % k)
	var a = Patron.find_item(&"age", k, &"creatures")
	if a == null:
		age = Vector2i(20, 30)
	else:
		age = a
	prices = Patron.find_item(&"price", k, &"creatures")

func make_age() -> int:
	return randi_range(age[0], age[1])

func make_name() -> StringName:
	return names.entries.pick_random().key_name

func make_hat() -> StringName:
	return Patron.get_hat()

func make_price() -> Price:
	return prices.concretize()
