extends Node

var starting := []
var reset := false

func _ready():
	for b in get_children():
		if b is RigidBody2D:
			starting.push_back({&"transform": b.transform, &"RID": b.get_rid()})

func do_reset():
	for b in starting:
		PhysicsServer2D.body_set_state(b.RID, PhysicsServer2D.BODY_STATE_LINEAR_VELOCITY, Vector2(randf_range(-200.0, 200.0), randf_range(-200.0, 200.0)))
		PhysicsServer2D.body_set_state(b.RID, PhysicsServer2D.BODY_STATE_ANGULAR_VELOCITY, 0.0)
		PhysicsServer2D.body_set_state(b.RID, PhysicsServer2D.BODY_STATE_TRANSFORM, b.transform)

func _unhandled_key_input(event):
	if event is InputEventKey and event.is_released():
		match event.keycode:
			KEY_R:
				reset = true
			KEY_ESCAPE:
				get_tree().quit()

func _process(_delta):
	if reset:
		reset = false
		do_reset.call_deferred()
