@tool
class_name FastOptionButton extends FastButton

@onready var optionspanel = PanelContainer.new()
@onready var list = VBoxContainer.new()
const innerbutton = preload("res://templates/inner_option_button.tscn")
const popup_panel_script = preload("res://templates/OptionButtonPanelContainer.gd")
signal selection_changed

@export var contain = []
var key
var selected
var f: Callable = func(s): return Gnomon.current_instance[key][s]

func _ready() -> void:
	optionspanel.size_flags_horizontal = Control.SIZE_SHRINK_BEGIN
	optionspanel.size_flags_vertical = Control.SIZE_SHRINK_BEGIN
	optionspanel.set_script(popup_panel_script)
	optionspanel.add_child(list)
	if not Engine.is_editor_hint():
		GUI.Game.add_child.call_deferred(optionspanel)
	optionspanel.z_index = 200
	optionspanel.hide()

func _notification(what: int) -> void:
	if (what == NOTIFICATION_CRASH\
	or	what == NOTIFICATION_EXIT_CANVAS\
	or	what == NOTIFICATION_EXIT_TREE\
	or	what == NOTIFICATION_UNPARENTED\
	or	what == NOTIFICATION_PREDELETE)\
	and is_instance_valid(optionspanel):
		optionspanel.queue_free()

func _pressed():
	if not optionspanel.visible:
		if not contain.is_empty():
			for n in list.get_children():
				list.remove_child(n)
				n.queue_free()
			for i in contain:
				var b = innerbutton.instantiate()
				b.text = f.call(i)
				b.pressed.connect(select.bind(i))
				list.add_child(b)
			optionspanel.show()
			await optionspanel.draw
			optionspanel.set_global_position.call_deferred(get_global_rect().position + Vector2(0, 50))

func select(id, emit = true):
	if key != null:
		selected = id
		if selected in Gnomon.current_instance[key]:
			text = f.call(selected)
			optionspanel.hide()
			if emit: selection_changed.emit()
