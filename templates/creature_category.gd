extends HBoxContainer
var ref:
	set(v):
		ref = v
		update.call_deferred()
var open = false
var open_tex = AtlasTexture.new()
var closed_tex = AtlasTexture.new()
var hover_box = preload("res://theme/hover_box.tres")

func _ready():
	var arrow = preload("res://theme/cat_arrow.png")
	open_tex.atlas = arrow
	closed_tex.atlas = arrow
	open_tex.region = Rect2(32, 0, 32, 32)
	closed_tex.region = Rect2(0, 0, 32, 32)
	find_child(&"Arrow").texture = open_tex
	update_arrow()
	hide()

func update():
	update_arrow()
	find_child(&"amount").ref = ref
	find_child(&"capacity").ref = ref + &"_max"

func toggle():
	open = not open
	update_arrow()
	if open:
		find_parent(&"Creatures").show_creature(ref)
	else:
		find_parent(&"Creatures").hide_creature(ref)

func update_arrow():
	if open:
		find_child(&"Arrow").texture = open_tex
	else:
		find_child(&"Arrow").texture = closed_tex

func _notification(what: int) -> void:
	if what == NOTIFICATION_MOUSE_ENTER:
		get_node("PanelContainer").add_theme_stylebox_override(&"panel", hover_box)
	if what == NOTIFICATION_MOUSE_EXIT:
		get_node("PanelContainer").remove_theme_stylebox_override(&"panel")

func _on_gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
			accept_event()
			toggle()
