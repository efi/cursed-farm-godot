extends HBoxContainer

@onready var icon = find_child(&"ResourceIcon")
@onready var L_type = find_child(&"Type")
@onready var L_dlc = find_child(&"DLC Label")
@onready var L_name = find_child(&"Name")
@onready var L_work = find_child(&"Work")
