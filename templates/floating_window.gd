class_name FloatingWindow extends PanelContainer

@export var title: String:
	set(v):
		%Label.text = v
		return true
	get():
		return %Label.text

@export var resizable: bool = false:
	set(v):
		resizable = v
		%resizer.visible = resizable
		return true

var initial_position: Callable = func() -> Vector2: return (get_viewport_rect().size - size) * 0.5

var contents: Control
var _held
var _resize: bool = false

func _ready() -> void:
	%resizer.visible = resizable
	contents = %contents
	for c in get_children():
		if c != %frame:
			c.reparent(contents)

func _on_close_pressed() -> void:
	hide()

func _notification(what: int) -> void:
	if is_node_ready() and what == NOTIFICATION_VISIBILITY_CHANGED:
		GUI.beep.play()
		position = initial_position.call()


func _on_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT:
		accept_event()
		if event.is_pressed():
			_held = get_global_mouse_position() - global_position
		elif event.is_released():
			_resize = false
			_held = null

func _process(_delta: float) -> void:
	if visible and _held != null:
		global_position = get_global_mouse_position() - _held
	if visible and _resize:
		size.y = get_global_mouse_position().y - global_position.y + 6.0


func _on_resizer_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT:
		accept_event()
		if event.is_pressed():
			_resize = true
		elif event.is_released():
			_resize = false
