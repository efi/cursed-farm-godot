@tool
@icon("res://debug/Label.svg")
class_name FastLabel extends Control

@export var text: String:
	set(v):
		if v == text: return true
		text = v
		if is_node_ready(): _write()
		return true

@export var label_settings: LabelSettings
var font_size: int
var font: Font
var color: Color:
	set(v):
		color = v
		queue_redraw()
		return true
var _line: TextLine = TextLine.new()
@export var alignment = HORIZONTAL_ALIGNMENT_LEFT

func _init() -> void:
	size_flags_vertical = Control.SIZE_SHRINK_CENTER
	mouse_filter = Control.MOUSE_FILTER_IGNORE

func _enter_tree() -> void:
	if label_settings != null:
		font_size = label_settings.font_size
		if label_settings.font != null:
			font = label_settings.font
		color = label_settings.font_color
	_write()

func _write() -> void:
	_line.clear()
	_line.add_string(text, get_theme_font("font") if font == null else font, get_theme_font_size("font_size") if font_size == 0 else font_size)
	update_minimum_size()
	queue_redraw()

func _get_minimum_size() -> Vector2:
	return _line.get_size()

func _draw() -> void:
	var p = Vector2.ZERO
	if alignment == HORIZONTAL_ALIGNMENT_RIGHT:
		p = Vector2(size.x - get_combined_minimum_size().x, 0.0)
	if alignment == HORIZONTAL_ALIGNMENT_CENTER:
		p = Vector2((size.x - get_combined_minimum_size().x) * 0.5, 0.0)
	_line.draw(get_canvas_item(), p, get_theme_color("font_color", "Label") if color == Color.BLACK else color)
	return
