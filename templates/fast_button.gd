@tool
class_name FastButton extends BaseButton

var _panel: PanelContainer = PanelContainer.new()
var _box: HBoxContainer = HBoxContainer.new()
var _label: FastLabel = FastLabel.new()

@export var text: String:
	set(v):
		_label.text = v
		update_size()
		return true
	get():
		if _label is not FastLabel:
			_label = FastLabel.new()
		return _label.text
@export var label_settings: LabelSettings
@export var text_alignment: HorizontalAlignment = HORIZONTAL_ALIGNMENT_LEFT
@export_enum("left", "right") var label_side: int = 0

@export_group(&"Style", &"style")
@export var style_normal: StyleBox
@export var style_pressed: StyleBox
@export var style_hover: StyleBox
@export var style_disabled: StyleBox
@export var style_focused: StyleBox

func _init() -> void:
	add_child(_panel)
	_panel.add_child(_box)
	_box.add_theme_constant_override(&"separation", 0)
	_box.add_child(_label)
	_label.size_flags_horizontal = Control.SIZE_EXPAND_FILL
	_label.size_flags_vertical = Control.SIZE_EXPAND_FILL

func _enter_tree() -> void:
	_panel.show_behind_parent = true
	_panel.mouse_filter = Control.MOUSE_FILTER_PASS
	_panel.custom_minimum_size = custom_minimum_size
	#_panel.size_flags_horizontal = Control.SIZE_EXPAND_FILL
	#_panel.size_flags_vertical = Control.SIZE_EXPAND_FILL
	_panel.set_anchors_preset(Control.PRESET_FULL_RECT)
	_label.mouse_filter = Control.MOUSE_FILTER_PASS
	_label.color = get_theme_color("font_color", "Button")
	_label.alignment = text_alignment
	_label.text = text
	_label.label_settings = label_settings
	if not Engine.is_editor_hint():
		for c in get_children():
			if c == _panel: continue
			c.reparent(_box)
		if label_side == 1:
			_box.move_child(_label, _box.get_child_count())
	update_size()
	if style_normal == null:
		style_normal = get_theme_stylebox("normal", "Button")
	if style_pressed == null:
		style_pressed = get_theme_stylebox("pressed", "Button")
	if style_hover == null:
		style_hover = get_theme_stylebox("hover", "Button")
	if style_disabled == null:
		style_disabled = get_theme_stylebox("disabled", "Button")
	if style_focused == null:
		style_focused = get_theme_stylebox("focus", "Button")

func update_size():
	#_panel.reset_size()
	_panel.update_minimum_size()
	update_minimum_size()
	queue_redraw()

func _get_minimum_size() -> Vector2:
	return custom_minimum_size.max(_panel.size)
	#return _panel.get_combined_minimum_size()


func current_style() -> StyleBox:
	match get_draw_mode():
		DrawMode.DRAW_NORMAL:
			return style_normal
		DrawMode.DRAW_HOVER:
			return style_hover
		DrawMode.DRAW_PRESSED:
			return style_pressed
		DrawMode.DRAW_HOVER_PRESSED:
			return style_pressed
		DrawMode.DRAW_DISABLED:
			return style_disabled
	return null

func _notification(what: int) -> void:
	if what == NOTIFICATION_RESIZED\
	or what == NOTIFICATION_DRAW:
		if is_node_ready() and get_tree().paused: return
		_panel.remove_theme_stylebox_override("panel")
		_label.color = get_theme_color("font_color", "Button")
		if has_focus():
			_label.color = get_theme_color("font_pressed_color", "Button")
		if is_hovered():
			_panel.add_theme_stylebox_override("panel", style_hover)
			_label.color = get_theme_color("font_hover_color", "Button")
		if button_pressed:
			_panel.add_theme_stylebox_override("panel", style_pressed)
			_label.color = get_theme_color("font_pressed_color", "Button")
		if disabled:
			_panel.add_theme_stylebox_override("panel", style_disabled)
			_label.color = get_theme_color("font_disabled_color", "Button")
		update_size()

func _draw():
	if has_focus():
		draw_style_box(style_focused, _panel.get_rect())
