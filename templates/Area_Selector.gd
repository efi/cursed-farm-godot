extends PanelContainer

@onready var dim = %Dimension
@onready var reg = %Region
signal switch_location

var location: Vector2i = Vector2i(0, 0)

func _ready():
	for c in get_children():
		if c == $HBoxContainer: continue
		c.reparent($HBoxContainer)
	update()

func update():
	dim.contain = Patron.map_dimensions()
	dim.key = &"dimension_name"
	dim.select(location[0], false)
	reg.contain = Patron.dimension_regions(location[0])
	reg.key = &"region_name"
	reg.select(location, false)

func dim_changed():
	reg.select(Vector2i(dim.selected, 0))

func reg_changed():
	location = reg.selected
	update()
	switch_location.emit()
