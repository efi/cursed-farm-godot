class_name ConstructionTimer extends Control

var timer: gTimer
var start: float
@export var verts: int = 32
var end: Callable
const color: Color = Color(0.847059, 0.643137, 0.505882, 0.75)#d8a481

func _ready():
	set_position(Vector2.ZERO)
	start = Gnomon.current_instance.date

func set_time(t: float):
	if is_node_ready():
		timer = gTimer.new(t)
	else:
		push_error(&"Using Construction Timer, but it's not ready")

func _process(_delta):
	if is_node_ready() and timer.complete():
		if end != null:
			end.call()
		get_parent().remove_child(self)
		queue_free()
	else:
		queue_redraw()

func _draw():
	#draw_rect(get_rect(), Color.GREEN)
	var center: Vector2 = position + size / 2.0
	var radius: float = 0.9 * min(size.x, size.y) / 2.0
	var completion: float = (Gnomon.current_instance.date - start) / (timer.date - start)
	var point_num: int = floori(verts * completion)
	var points: PackedVector2Array = [center, center + Vector2(1.0, 0.0) * radius]
	for p in range(1, point_num - 1):
		points.push_back(center + Vector2(cos(TAU * float(p) / float(verts)), sin(TAU * float(p) / float(verts))) * radius)
	points.push_back(center + Vector2(cos(TAU * completion), sin(TAU * completion)) * radius)
	draw_colored_polygon(points, color)
	draw_polyline(points.slice(1), Color.BEIGE, 1.5, true)
