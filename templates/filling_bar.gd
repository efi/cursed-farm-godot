class_name FillingBar extends PanelContainer

#@onready var filling := %Filling
@export var overlay: StyleBox
var filling: Control
var value: Callable = func(): return 0.0:
	set(v):
		value = v
		queue_redraw()
		return true
var max_value: Callable = func(): return 0.0:
	set(v):
		value = v
		queue_redraw()
		return true
var _pv: float
var _pmv: float

func _init():
	filling = Panel.new()

func _process(_delta):
	if is_node_ready() and visible and Session.is_active() and max_value != Callable() and value != Callable():
		var mv := float(max_value.call())
		var v := float(value.call())
		filling.size.y = size.y
		if v <= 0.0:
			filling.hide()
		else:
			filling.show()
			filling.size.x = size.x * (v / mv if v < mv else 1.0)
			if _pv != v or _pmv != mv:
				queue_redraw()
				_pv = v
				_pmv = mv

func _draw():
	draw_style_box(overlay, filling.get_rect())
