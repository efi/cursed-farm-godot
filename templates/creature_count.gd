extends PanelContainer

var ref:
	set(v):
		ref = v
		update.call_deferred(v)
		return true

func update(v):
	get_node(^"%Resource Icon").icon = v
	var display = Patron.find_item(&"plural", v, &"display_names")
	get_node(^"%name").text = display if display != null else &"#ERROR"
	get_node(^"%count").ref = StringName(&"creatures.%s" % v)
	get_node(^"%housing").ref = StringName(&"storage.%s" % v)
