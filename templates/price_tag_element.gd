extends HBoxContainer

func update(e, v):
	get_node(^"%Resource Icon").icon = e
	get_node(^"%Label").text = &"x" + str(v)
	show()
