@tool
class_name ResourceIcon extends TextureRect
@export var icon: StringName = &"":
	set(v):
		icon = v
		update.call_deferred()
		return true

func update():
	if Engine.is_editor_hint():
		texture = load("res://debug/var_icon.png")
		return
	if icon == null or icon.is_empty():
		hide()
		texture = null
		return
	show()
	var loc = Patron.locate_sprite(icon)
	if loc == null:
		texture = null
	else:
		texture = Patron.sprite(loc, icon)
