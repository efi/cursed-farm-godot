extends Button

var creature

func _process(_delta):
	if Session.is_active() and not creature == null:
		disabled = not Patron.can_hire(creature)

func _pressed():
	if Patron.try_hire(creature):
		get_node(^"../../..").hide()
		GUI.beep.play()
