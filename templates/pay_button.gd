extends Button

@onready var _price = $price_tag

func _on_price_tag_updated():
	custom_minimum_size = Vector2.ZERO
	reset_size()
	custom_minimum_size = Vector2(size.x + _price.size.x, size.y)
	reset_size()
	_price.position = Vector2(size.x - _price.size.x, (size.y - _price.size.y) * 0.5)
