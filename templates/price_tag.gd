@icon("res://debug/price.png")
extends HBoxContainer
const el = preload("res://templates/price_tag_element.tscn")

signal updated

var price:
	set(v):
		assert(v is Price)
		price = v
		recreate.call_deferred()

func recreate():
	for e in get_children():
		remove_child(e)
		e.queue_free()
	for c in price.price:
		for i in price.price[c]:
			var e = el.instantiate()
			e.update(i, price.price[c][i])
			add_child(e)
	reset_size()
	updated.emit()

func _process(_delta):
	if Session.is_active() and price != null:
		if Patron.can_pay(price):
			get_parent().self_modulate = Color.WHITE
		else:
			get_parent().self_modulate = Color.RED
