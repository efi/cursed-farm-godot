extends PanelContainer

func _input(event):
	if 	visible\
		and event is InputEventMouseButton\
		and event.is_released()\
		and event.button_index == MOUSE_BUTTON_LEFT\
		and not get_global_rect().has_point(event.global_position):
			hide.call_deferred()
