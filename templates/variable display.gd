@tool
@icon("res://debug/var_icon.png")
class_name VariableDisplay extends FastLabel

var ref:
	set(v):
		if Engine.is_editor_hint(): return false
		assert(v is StringName or v is Callable)
		if v is StringName:
			var ce = CheekyExpression.parse_address(v)
			ref = Patron.item_count.bind(ce.collection, ce.item)
		elif v is Callable:
			ref = v
		return true
var last_update = 0.0

func _process(_delta):
	if not Engine.is_editor_hint()\
	and is_node_ready()\
	and visible\
	and Session.is_active()\
	and Patron.date_passed(last_update):
		queue_redraw()
		last_update = Patron.current_date() + 0.2
		if ref:
			text = Patron.num_to_str(ref.call())
		else:
			text = &"#ERROR"
