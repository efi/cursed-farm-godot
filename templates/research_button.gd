extends FastButton
var res = null:
	set(v):
		res = v
		update.call_deferred()

func update():
	text = Patron.find_item(&"name", res, &"research_names")

func show_research():
	GUI.ResearchGUI.req_side.show()
	GUI.ResearchGUI.req_side.find_child("Name").text = Patron.find_item(&"name", res, &"research_names") #Patron.research_names[res].name
	GUI.ResearchGUI.req_side.find_child("Description").text = Patron.find_item(&"description", res, &"research_names") #Patron.research_names[res].description
	GUI.ResearchGUI.requirements.price = Patron.find_item(&"price", res, &"research") #Gnomon.research[res].get_price()
