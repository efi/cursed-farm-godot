@tool
extends FastButton
@export var DLC_Name = &"DLC Name":
	set(v):
		if DLC_Name != v:
			DLC_Name = v
			%text.text = v
		return true
@export var DLC_Icon: Texture2D:
	set(v):
		if DLC_Icon != v:
			DLC_Icon = v
			%icon.texture = DLC_Icon
		return true
