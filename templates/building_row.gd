extends PanelContainer
var ref:
	set(v):
		ref = v
		update.call_deferred()

func update():
	if not ref.is_empty():
		%ResourceIcon.icon = ref
		%Name.text = Patron.find_item(&"plural", ref, &"display_names") #display_names.plural[ref]
		%working.ref = Gnomon.current_instance.count_works_at.bind(ref)
		%FillingBar.max_value = Gnomon.current_instance.building_slots_total.bind(ref)
		%FillingBar.value = Gnomon.current_instance.count_works_at.bind(ref)
		%available_w.ref = Gnomon.current_instance.free_workers.bind(ref)
		%available_s.ref = Gnomon.current_instance.building_slots_total.bind(ref)

func increase():
	var total_slots: int = Gnomon.current_instance.building_slots_total(ref)
	var free_workers: int = Gnomon.current_instance.free_workers(ref)
	if total_slots > 0 and free_workers > 0:
		var slotted_workers: int = Gnomon.current_instance.count_works_at(ref)
		if slotted_workers < total_slots:
			for b in Gnomon.current_instance.realized(&"buildings", ref):
				for s in len(b.contents):
					if b.contents[s] == null:
						var n = next_free_worker_id()
						if n != null:
							GUI.beep.play()
							b.contents[s] = n
							Gnomon.current_instance.works_in[n] = b
							Gnomon.current_instance.workforce[ref] += 1
							return

func next_free_worker_id():
	for k in Patron.find_item(&"in", ref, &"buildings"):
		for c in Gnomon.current_instance.realized(&"creatures", k):
			if not Gnomon.current_instance.is_working(c):
				return c.UID

func decrease() -> void:
	if Gnomon.current_instance.workforce[ref] > 0:
		Gnomon.current_instance.workforce[ref] -= 1
		GUI.beep.play()
		for b in Gnomon.current_instance.buildings[ref].manual:
			for c in b.contents:
				if c != null:
					var i = b.contents.find(c)
					b.contents[i] = null
					if not Gnomon.current_instance.works_in.erase(c):
						push_warning(&"%s removed as worker from %s, but not in 'works_in' list" % [c, b])
					GUI.beep.play()
					return

func details():
	GUI.Main.find_child(&"CreatureDetailPopup").building = ref
	GUI.Main.find_child(&"CreatureDetailPopup").show()
