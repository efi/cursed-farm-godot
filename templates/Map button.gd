extends Button

@onready var forbidden = get_node("forbidden")
@onready var pop = GUI.Game.get_node(^"sounds/pop")

var index

func _ready():
	link_context_GUI.call_deferred()

func link_context_GUI():
	mouse_entered.connect(GUI.MapGUI.context.update.bind(index))

func forbidden_outline(v = &""):
	if v.is_empty() or Gnomon.current_instance.map[GUI.MapGUI.current_display].tile_get(index).can_build(v):
		forbidden.hide()
	else:
		forbidden.show()

func update():
	var disp = Gnomon.current_instance.map[GUI.MapGUI.current_display].tile_get(index).display()
	var loc = Patron.locate_sprite(disp)
	var spr: AtlasTexture = Patron.sprite(loc, disp)
	if spr:
		icon = spr
	else:
		icon = null

func _pressed():
	if GUI.MapGUI.build.build_mode:
		GUI.MapGUI.action(GUI.MapGUI.build.which, index)
		return true
	else:
		GUI.Tooltip.hide()
		GUI.MapGUI.action(Gnomon.current_instance.map[GUI.MapGUI.current_display].tile_get(index).display(), index)
		return true

func _notification(what: int) -> void:
	if what == NOTIFICATION_FOCUS_ENTER:
		z_index = 1
	elif what == NOTIFICATION_FOCUS_EXIT:
		z_index = 0
