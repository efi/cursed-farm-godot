extends Button

var index: int
var clicked: Vector2
var relpos: Vector2 = Vector2.ZERO
var original_pos: Vector2
@onready var cauldron := find_parent(&"Cauldron")
@onready var move := $move
var ref: StringName:
	set(v):
		ref = v
		if v in Patron.sprites:
			$TextureRect.texture = Patron.sprites[v]
			if v.begins_with(&"alc_"):
				$TextureRect.modulate = Color.GOLD
			else:
				$TextureRect.modulate = Color.WHITE
			return true

func _gui_input(event):
	if  event is InputEventMouseMotion:
		accept_event()
		set_tooltip()
	if  event is InputEventMouseButton\
		and event.button_index == MOUSE_BUTTON_LEFT\
		and cauldron.swaps > 0:
		if event.is_pressed():
			accept_event()
			clicked = get_local_mouse_position()
		if event.is_released() and not cauldron.locked:
			accept_event()
			var rp = get_local_mouse_position() - clicked
			if not rp.is_zero_approx():
				release_focus()
				var w = cauldron.grid_width()
				var h = cauldron.grid_height()
				var anim := -1
				cauldron.locked = true
				move.play()
				if absf(rp.x) > absf(rp.y):
					if rp.x < 0 and GridLimit.west(index, w, h) != -1:
						anim = 0
					elif rp.x > 0 and GridLimit.east(index, w, h) != -1:
						anim = 2
				else:
					if rp.y < 0 and GridLimit.north(index, w, h) != -1:
						anim = 1
					elif rp.y > 0 and GridLimit.south(index, w, h) != -1:
						anim = 3
				match anim:
					0:
						get_parent().get_child(GridLimit.west(index, w, h)).anim_side(&"GUI-anim/Swap-right")
						anim_side(&"GUI-anim/Swap-left")
					1:
						get_parent().get_child(GridLimit.north(index, w, h)).anim_side(&"GUI-anim/Swap-down")
						anim_side(&"GUI-anim/Swap-up")
					2:
						get_parent().get_child(GridLimit.east(index, w, h)).anim_side(&"GUI-anim/Swap-left")
						anim_side(&"GUI-anim/Swap-right")
					3:
						get_parent().get_child(GridLimit.south(index, w, h)).anim_side(&"GUI-anim/Swap-up")
						anim_side(&"GUI-anim/Swap-down")
					_:
						cauldron.locked = false

func _process(_delta):
	if $AnimationPlayer.is_playing():
		position = original_pos + relpos

func anim_side(s: StringName):
	original_pos = position
	$AnimationPlayer.play(s)

func finish_swap(anim):
	position = original_pos
	relpos = Vector2.ZERO
	cauldron.locked = false
	match anim:
		&"GUI-anim/Swap-right":
			cauldron.swap(index, GridLimit.east)
		&"GUI-anim/Swap-up":
			cauldron.swap(index, GridLimit.north)
		_:
			pass

func _notification(what: int) -> void:
	if what == NOTIFICATION_MOUSE_ENTER:
		set_tooltip()

func set_tooltip():
	GUI.Tooltip.update(ref, ref)
	GUI.Tooltip.position = get_screen_position() + size / 2 - Vector2(GUI.Tooltip.size.x / 2, GUI.Tooltip.size.y * 1.5)
	GUI.Tooltip.show()
