class_name MapWindow extends GridContainer

@onready var CT = preload("res://templates/construction_timer.tscn")
@onready var Map_button = preload("res://templates/map_button.tscn")
var location: Vector2i = Vector2i.ZERO
var map_size: int = 13

func _enter_tree() -> void:
	if get_child_count() == 0:
		columns = map_size
		for i in map_size * map_size:
			var b = Map_button.instantiate()
			b.custom_minimum_size = Vector2(32, 32)
			b.index = i
			add_child(b)
		setup_focus_neighbors()

func setup_focus_neighbors():
	for b in get_children():
		b.set_focus_neighbor(SIDE_RIGHT,	b.get_path_to(get_child(b.index + 1))			if posmod(b.index + 1, map_size)	> 0						else ^"")
		b.set_focus_neighbor(SIDE_TOP,		b.get_path_to(get_child(b.index - map_size))	if b.index - map_size				> 0						else ^"")
		b.set_focus_neighbor(SIDE_LEFT,		b.get_path_to(get_child(b.index - 1))			if posmod(b.index - 1, map_size)	< map_size - 1			else ^"")
		b.set_focus_neighbor(SIDE_BOTTOM,	b.get_path_to(get_child(b.index + map_size))	if b.index + map_size				< map_size * map_size	else ^"")

func get_map(at: Vector2i) -> Map:
	return Gnomon.current_instance.map[at]

func find_slot_filter(filt: Callable) -> int:
	var m: Map = get_map(location)
	var i = range(m.length - 1)
	i = i.filter(func(n: int): return filt.call(m.tile_get(n)))
	if not i.is_empty():
		return i.pick_random()
	return -1

func find_mushroom_slot():
	var m: Map = get_map(location)
	return find_slot_filter(
		func(c: MapComponent):
			return m.exactly(c, MapTemplate.FOREST))

func find_belladona_slot() -> int:
	var m: Map = get_map(location)
	return find_slot_filter(
		func(c: MapComponent):
			return 	m.exactly(c, MapTemplate.PLAINS) and \
					m.any_neighbor(c, MapTemplate.FOREST))

func slot_update(n: int, thing: StringName) -> void:
	if n < 0: return
	get_map(location).tile_add(n, thing)
	get_child(n).update()

func slot_remove(n: int, thing: StringName) -> void:
	if n < 0: return
	get_map(location).tile_remove(n, thing)
	get_child(n).update()

func slot_replace(n: int, thing: StringName) -> void:
	if n < 0: return
	get_map(location).tile_replace(n, thing)
	get_child(n).update()
