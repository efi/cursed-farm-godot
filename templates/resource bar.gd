extends PanelContainer
@export var ref: StringName:
	set(v):
		ref = v
		if Patron.collection_has_key(&"collectables", ref):
			setup.call_deferred()
		else:
			push_error(&"Wrong ref %s at %s" % [ref, self])

func setup():
	%amount.ref = Patron.item_count.bind(&"collectables", ref)
	%capacity.ref = Patron.storage_for.bind(ref)
	%rate.ref = Session.rate_for.bind(ref)
	%FillingBar.max_value = Patron.item_count.bind(&"storage", ref)
	%FillingBar.value = Patron.item_count.bind(&"collectables", ref)
	%ResourceIcon.icon = ref

func _on_mouse_entered() -> void:
	if not ref.is_empty():
		GUI.Tooltip.update(ref, ref)
		GUI.Tooltip.position = global_position + Vector2(size.x, 0)
		GUI.Tooltip.show()
		return

func _on_mouse_exited() -> void:
	if GUI.Tooltip != null: GUI.Tooltip.hide()
	return
