extends HBoxContainer

@onready var icon: Node = get_node(^"%Resource Icon")
@onready var L_type: Node = get_node(^"%Type")
@onready var L_dlc: Node = get_node(^"%DLC Label")
@onready var L_name: Node = get_node(^"%Name")
@onready var L_age: Node = get_node(^"%Age")
@onready var price: Node = get_node(^"%price_tag")
@onready var hire: Node = get_node(^"%HIRE")
