extends ScrollContainer

func _ready():
	get_v_scroll_bar().focus_mode = Control.FOCUS_CLICK

func _process(_delta):
	if not get_v_scroll_bar().has_focus() and not has_focus() and get_child(0).get_child_count() > 0:
		ensure_control_visible(get_child(0).get_child(get_child(0).get_child_count() - 1))

func _notification(what: int) -> void:
	if what == NOTIFICATION_MOUSE_ENTER:
		grab_focus()
