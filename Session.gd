class_name Session extends RefCounted

enum {MAIN_MENU, STARTING, ACTIVE, QUITTING}

static var state := MAIN_MENU
static var timers := {
		&"status": gTimer.new(0.2),
		&"amanita": gTimer.new(1.0),
		&"wood": gTimer.new(20.0),
		&"candidates": gTimer.new(0.1),
		&"belladona": gTimer.new(0.1),
	}
static var rate: Array[MutatorRate] = []# resource rates
static var noise := FastNoiseLite.new()
static var paused := true
static var debug: Array[String]
static var _callback := {}

static func on_60th_frame() -> bool:
	return Engine.get_process_frames() % 60 == 0

static func debug_line(l: String) -> void:
	if is_active():
		debug.push_back(l)

static func add_once_timer(name: StringName, time: float, callback: Callable):
	if name not in timers:
		timers[name] = gTimer.new(time)
		_callback[name] = callback

static func _event_complete(event: StringName) -> bool:
	return Patron.is_event_complete(event)

static func _researched(k, n = 1):
	return Patron.is_research_level_complete(k, n)

#static func provide(k_building, k_res, amount):
	#Patron.fset(k_res + &"_max", Patron.find(k_res + &"_max") + Patron.find(k_building) * amount)

static func rate_for(item: StringName) -> float:
	var r := 0.0
	for entry in rate:
		if entry.kind == Patron.locate(item) and entry.item == item:
			r += entry.amount
	return r

static func process_rates(delta: float) -> void:
	for entry in rate:
		#printt(res, Patron.find(res), Patron.resource_types[res])
		#Patron.fset(res + &"_rate", rates[res])
		if entry.amount != 0.0 and entry.mod != 0.0:
			var current_count = Patron.item_count(entry.kind, entry.item)
			Gnomon.current_instance[entry.kind][entry.item] = max(0.0, min(max(Patron.storage_for(entry.item), current_count), current_count + entry.amount * entry.mod * delta))
			#debug_line(&"%s.%s %s/%s » %s (%s)" % [str(entry.kind), str(entry.item), str(current_count), str(Patron.storage_for(entry.item)), str(current_count + entry.amount * entry.mod * delta), str(delta)])

static func quit():
	print(&"Quitting...")
	state = QUITTING
	Engine.get_main_loop().quit()

static func _static_init():
	Patron.giddy_up()
	Sensory.add_sensor(&"temperature", func(): return Gnomon.current_instance.temperature)
	Sensory.add_sensor(&"humidity", func(): return Gnomon.current_instance.humidity)
	Sensory.add_sensor(&"weather", func(): return Gnomon.current_instance.weather)
	Sensory.add_sensor(&"bonfire_wood", func(): return Gnomon.current_instance.bonfire.wood)
	Sensory.add_sensor(&"bonfire_heat",  func(): return Gnomon.current_instance.bonfire.heat)
	noise.seed = 0#randomize in actual run
	noise.noise_type = FastNoiseLite.TYPE_VALUE


static func is_active() -> bool:
	return state == ACTIVE

static func S(s: StringName):
	return Sensory.sense(s)

static func obtain_rates() -> Array[MutatorRate]:
	var rates: Array[MutatorRate] = []
	for e in Patron.mutators.entries:
		var M = e.values
		var input = CheekyExpression.parse_address(M.input)
		if Patron.item_count(input.collection, input.item) > 0.0:
			var output = CheekyExpression.parse_address(M.output)
			if 	(e.values.multiplier > 0 and Patron.item_count(output.collection, output.item) < Patron.storage_for(output.item)) or\
				(e.values.multiplier < 0 and Patron.item_count(output.collection, output.item) > 0):
				if M.requirement.check():
					var input_count: float = Patron.item_count(input.collection, input.item)
					rates.push_back(MutatorRate.new(output.collection, output.item, pow(input_count * M.multiplier, M.exponent), 1.0))
	return rates

static func tick(delta):
	debug.clear()
	#for t in timers.keys():
		#debug_line(&"T:%s: %s" % [t, timers[t].date])
	#region == TIME ==
	var gnomon = Gnomon.current_instance
	var date_delta = delta * gnomon.time_stretch
	gnomon.date += date_delta
	Sensory.update(gnomon.date)
	if timers.amanita.complete():
		if S(&"temperature") <= 0.0:
			timers.amanita.reset(1.0)
		else:
			timers.amanita.reset(4.0)
			GUI.MapGUI.spawn_mushroom()
	if timers.wood.complete():
		timers.wood.reset(5.0)
		GUI.MapGUI.spawn_wood()
	if _event_complete(&"belladona") and timers.belladona.complete():
		timers.belladona.reset(10.0)
		GUI.MapGUI.spawn_belladona()
	if timers.candidates.complete():
		timers.candidates.reset(10.0)
		GUI.Creatures.partial_refresh_candidate_list()
	#endregion
	#region == GAME EVENTS ==
	for ev in Patron.game_events:
		Patron.game_events[ev].check()
	for once in _callback.keys():
		if timers[once].complete():
			_callback[once].call()
			_callback.erase(once)
			timers.erase(once)
	#endregion
	#region == STORAGE ==
	#for k in gnomon.collect_limit:
		#gnomon.collect_limit[k] = 0
	#for k in gnomon.creature_limit:
		#gnomon.creature_limit[k] = 0
	#for storage in Patron.storage:
		#Patron.fset(storage.provides + &"_max", 0)
	#for storage in Patron.storage:
		#provide(storage.key, storage.provides, storage.amount)
	#Gnomon.collect_limit.wood_max = max(10, Gnomon.collect_limit.wood_max)
	#endregion
	#region == PRODUCTION AND CONSUMPTION ==
	rate = obtain_rates()
	
	for GM in ModifierStack.category(&"rate"):
		GM.actions().call()
	
	process_rates(date_delta)
	#endregion
	#region == BONFIRE ==
	var bon_wood = gnomon.bonfire.wood
	var bon_heat = gnomon.bonfire.heat
	if _researched(&"bonfire"):
		if Patron.item_count(&"buildings", &"bonfire") == 0:
			gnomon.bonfire.wood = 0.0
			gnomon.bonfire.heat = 0.0
		elif bon_wood > 0.0:
			gnomon.bonfire.heat = lerpf(bon_heat + pow(bon_wood, 1.2) * date_delta, 0.0, date_delta / 20.0)
			gnomon.bonfire.wood = lerpf(bon_wood, 0.0, date_delta / 40.0)
	#endregion
	#region == CLIMATE ==
	#TODO: Bundle climate into a class so each region can have a different one
	var n = noise.get_noise_1d(gnomon.date)
	var ToY = fmod(gnomon.date, 3600.0)# time of year loops each hour
	#ToY = find_child(&"dbgControl").find_child(&"time").value
	var basetemp = n * 5.0 + sin(TAU * ToY / 3600.0 - 120.0) * 15.0 + sin(TAU * ToY / 120.0) * 12.0 + 15.0# a day is 2 minutes
	var basehumid = n * 10.0 + sin(TAU * ToY / 3600.0 + TAU/4.0) * 45.0 + 50.0
	var t: float
	var h: float
	var w: float
	#GUI.Status.find_child(&"dbg").text = &"%s %s %s " % [floori(ToY), floori(basetemp), floori(basehumid)]
	var rain = 0
	if posmod(floori(ToY / 900.0), 2) != 0:	rain += 3
	if S(&"humidity") > 95:							rain += 1
	if S(&"humidity") > 80:							rain += 1
	if S(&"humidity") > 50:							rain += 1
	if S(&"weather") > 2 and S(&"weather") < 5:		rain += 1
	if S(&"humidity") < 40:							rain -= 1
	if floori(ToY / 900.0) == 2:					rain -= 1
	if S(&"temperature") <  0:						rain -= 2
	if w > 2 and S(&"temperature") > 0:
		h = lerp(S(&"humidity"), 100.0, date_delta / 10.0)
		t = lerp(S(&"temperature"), 0.0, date_delta / 10.0)
	w = clampi(int(sin(TAU * ToY / 200) * 1.5 * rain), 0, 4)
	if h < 50 and w > 2: w = 5# thunder
	if t < 0 and w > 3: w = 7# hail
	elif t < 0 and w > 2: w = 6# snow
	gnomon.weather = floori(w)
	var out = GUI.Game.find_child(&"dbgOut")
	out.clear()
	out.append_text(&"Time: %s" % ToY)
	out.newline()
	#out.append_text(&"TempBase: %s" % basetemp)
	#out.newline()
	out.append_text(&"Temp: %s" % t)
	out.newline()
	#out.append_text(&"HumiBase: %s" % basehumid)
	#out.newline()
	out.append_text(&"Humi: %s" % h)
	out.newline()
	out.append_text(&"Rain: %s" % rain)
	out.newline()
	#out.append_text(&"Weather: %s" % w)
	#out.newline()
	#out.append_text(&"Result: %s" % Patron.weather[w])
	#out.pop_all()
	t = lerpf(t, max(t, pow(bon_heat * 6.0 / 20.0, 0.8)), date_delta / 40.0)
	gnomon.temperature = lerpf(t, basetemp, date_delta * 30.0)
	gnomon.humidity = clampf(lerpf(h - ((log(bon_heat) if bon_heat > 0.0 else 0.0) * 3.0 * date_delta), basehumid, date_delta / 10.0), 0.0, 100.0)
	#debug_line(&"basetemp: %s" % basetemp)
	#debug_line(&"actualtemp: %s" % S(&"temperature"))
	#debug_line(&"humid: %s" % S(&"humidity"))
	#debug_line(&"rain: %s" % rain)
	#debug_line(&"weather: %s" % S(&"weather"))
	if timers.status.complete():
		timers.status.reset(0.2)
		if _researched(&"thermometer"):
			GUI.Status.get_node(^"%Temperature").text = str(floori(t)) + &"C"
		else:
			GUI.Status.get_node(^"%Temperature").text = Patron.temperature_word(Sensory.sense(&"temperature"))
		GUI.Status.get_node(^"%Weather").text = Patron.weather_word(gnomon.weather)
	#endregion
	#region == ASTROLOGY ==
	var constellation = floori(ToY / 300)# a month is 5 minutes
	var element = floori(ToY / 900)
	GUI.Status.find_child(&"dbg").text =\
			Patron.astro_name[constellation]\
			+ &" "\
			+ [&"air", &"earth", &"fire", &"water"][element]
	#endregion
