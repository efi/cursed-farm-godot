class_name MapTemplate extends Object

const PLAINS = &"plains"
const SEA = &"sea"
const MOUNTAINS = &"mountains"
const FOREST = &"forest"
const ENTRANCE = &"entrance"
const MINE_RESOURCES = [&"stone", &"coal", &"iron", &"copper", &"tin", &"silver", &"gold"]
var tile: Callable
var post: Callable

func generate(size: int) -> Array[MapComponent]:
	var map: Array[MapComponent] = []
	map.resize(size * size)
	for i in len(map):
		map[i] = MapComponent.new()
		map[i].setindex(i, size)
	for i in len(map):
		map[i].replace(tile.call(map[i].row, map[i].col, size))
	return post.call(map, size)

func _init(fnA: Callable, fnB: Callable = func(map): return map):
	tile = fnA
	post = fnB
