extends VBoxContainer

const OK = &"✓"
const NOK = &"✗"
@onready var label: RichTextLabel = get_node(^"%Label")
@onready var price_label = get_node(^"%Price")

func update(index: int):
	var BM = GUI.MapGUI.build.build_mode
	var ct: MapComponent = Gnomon.current_instance.map[GUI.MapGUI.current_display].tile_get(index) if index >= 0 else null #current tile
	var cb = false #can build?
	var cd = &"No tile" #current display
	if ct != null:
		if BM: cb = ct.can_build(GUI.MapGUI.build.which)
		if Patron.collection_has_key(&"collectables", ct.display()):
			cd = ct.contents.front()
		else:
			cd = ct.display()
	var loc = Patron.locate_sprite(cd)
	get_node(^"%ContextIcon").texture = null if loc == null else Patron.sprite(loc, cd)
	label.clear()
	label.push_paragraph(HORIZONTAL_ALIGNMENT_CENTER)
	var disp = Patron.find_item(&"singular", cd, &"display_names")
	if disp == null or disp.is_empty():
		disp = cd
	label.append_text(disp)
	label.newline()
	if BM:
		label.append_text(&"Building:")
		label.newline()
		if cb:
			label.push_color(Color.GREEN)
		else:
			label.push_color(Color.RED)
		label.append_text(Patron.find_item(&"singular", GUI.MapGUI.build.which, &"display_names")) #display_names.singular[GUI.MapGUI.build.which])
		label.newline()
		if cb:
			label.push_normal()
			label.append_text(OK)
		else:
			label.append_text(NOK)
	label.newline()
	if ct != null: label.append_text(str(ct.contents))
	label.pop_all()
	if BM:
		price_label.find_child("price_tag").price = Patron.building_ts[GUI.MapGUI.build.which].price
		price_label.show()
	else:
		price_label.hide()
	if  cd != &"No tile" \
		and not Gnomon.current_instance.map[GUI.MapGUI.current_display].tile_get(index).contents.is_empty() \
		and not Gnomon.current_instance.map[GUI.MapGUI.current_display].tile_get(index).display().begins_with(&"terrain_"):
			GUI.Tooltip.update(Gnomon.current_instance.map[GUI.MapGUI.current_display].tile_get(index).display(), Gnomon.current_instance.map[GUI.MapGUI.current_display].tile_get(index).display())
			GUI.Tooltip.show()
			GUI.Tooltip.position = GUI.MapGUI.Map_display.get_child(index).global_position - (GUI.Tooltip.size / 2) + Vector2(16, 16) - Vector2(0, GUI.Tooltip.size.y)
