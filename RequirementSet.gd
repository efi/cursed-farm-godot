class_name RequirementSet extends Resource

@export var reqs: Array[Requirement]

static func placeholder() -> RequirementSet:
	return RequirementSet.new()

func _init():
	parse_all()

func parse_all():
	for s in reqs:
		s.parse()

func check() -> bool:
	for r in reqs:
		if r.check() != true:
			return false
	return true

func _to_string():
	return str(&"RS[ %s ]" % str(reqs))
