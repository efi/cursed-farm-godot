class_name GameModifier extends RefCounted

enum WHEN {START, ADD, TICK, REMOVE, END}

var timed := false
var timer: gTimer
var kind: StringName
var amount: int
var visible: bool

func _init(k: StringName, vis := false):
	visible = vis
	var time = ModifierTemplate.duration(k)
	timer = gTimer.new(time)
	if time != 0.0:
		timed = true
	kind = k
	amount = 1

func expired():
	if timed:
		return timer.complete()
	else:
		return false

func categories() -> PackedStringArray:
	return ModifierTemplate.categories(kind)

func stacks() -> ModifierTemplate.STACKING:
	return ModifierTemplate.stacks(kind)

func removes() -> ModifierTemplate.REMOVING:
	return ModifierTemplate.removes(kind)

func actions(when_kind: WHEN = WHEN.TICK) -> Callable:
	return ModifierTemplate.actions(kind)[_when_kind(when_kind)]

func removal_fn() -> Spark:
	return ModifierTemplate.removal_fn(kind)

func _when_kind(when_k: WHEN) -> StringName:
	match when_k:
		WHEN.START: return &"start"
		WHEN.ADD: return &"add"
		WHEN.TICK: return &"tick"
		WHEN.REMOVE: return &"remove"
		WHEN.END: return &"end"
		_:
			push_error(&"WRONG WHEN KIND in game modifier action")
			return &"ERROR"
