extends Control
var bar = preload("res://templates/resource bar.tscn")
#@onready var bar: InstancePlaceholder
@onready var list = %resource_list
var _nodes: Array[Control]

func _ready():
	Timing.start(&"resources ready")
	$"Floating Window".initial_position = func() -> Vector2: return Vector2.ZERO
	for c in Patron.collectables.entries:
		var b = bar.instantiate()
		b.ref = c.key_name
		_nodes.push_back(b)
	_nodes.reverse()
	Timing.check(&"resources ready")

func _process(_delta: float) -> void:
	if not _nodes.is_empty() and is_node_ready():
		list.add_child(_nodes.pop_back())
		if _nodes.is_empty():
			update_list()

func show_resource(ref):
	Gnomon.current_instance.visible_resources.push_back(ref)
	update_list()

func update_list():
	for n in list.get_children():
		if n is Control:
			n.hide()
	for n in list.get_children():
		if n is Control and n.ref in Gnomon.current_instance.visible_resources:
			n.show()
