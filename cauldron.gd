extends PanelContainer

const tiers := [
	Vector2i(5, 3)
]
@onready var grid: Node = find_child(&"Grid")
@onready var swaps_label: Node = find_child(&"swaps")
@onready var slime: Node = find_child(&"Slime")
@onready var ingredients: Node = find_child(&"Ingredients")
@onready var boiling := $boiling
@onready var pop := $pop
var _but := preload("res://templates/cauldron_button.tscn")
var tier := 0
var values: PackedStringArray
var locked := false
var swaps := 0
var ingredient_count: Callable

func _ready():
	grid.columns = grid_width()
	values.resize(grid_size())
	values.fill(&"alc_aer")
	for i in grid_size():
		var b = _but.instantiate()
		b.index = i
		b.custom_minimum_size = Vector2i(32, 32)
		grid.add_child(b)
	slime.find_child(&"price_tag").price = Patron.price({&"collectables": {&"slime": 8}})
	ingredients.get_node("Amanita").find_child(&"price_tag").price = Patron.price({&"collectables": {&"amanita": 8}})
	ingredients.get_node("Belladona").find_child(&"price_tag").price = Patron.price({&"collectables": {&"belladona": 8}})
	update_all()

func _process(_delta):
	if Session.state == Session.ACTIVE and is_node_ready():
		if swaps == 0:
			boiling.volume_db = lerp(boiling.volume_db, -80.0, 0.005)
			if boiling.volume_db < -70.0:
				boiling.stop()
			slime.disabled = true
			for i in ingredients.get_children():
				i.disabled = true
		else:
			if not boiling.playing:
				boiling.play()
				boiling.volume_db = -80.0
			boiling.volume_db = lerp(boiling.volume_db, -10.0, 0.02)
			slime.disabled = not slime.find_child(&"price_tag").price.can_pay()
			ingredients.get_node("Amanita").disabled =\
				not ingredients.get_node("Amanita").find_child(&"price_tag").price.can_pay()
			ingredients.get_node("Belladona").disabled =\
				not ingredients.get_node("Belladona").find_child(&"price_tag").price.can_pay()

func grid_width() -> int:
	return tiers[tier][0]

func grid_height() -> int:
	return tiers[tier][1]

func grid_size() -> int:
	return tiers[tier][0] * tiers[tier][1]

func start():
	GUI.beep.play()
	swaps = 5#TODO
	for c in grid.get_children():
		grid.remove_child(c)
		c.queue_free()
	grid.columns = grid_width()
	values.resize(grid_size())
	for i in len(values):
		values[i] = &"alc_aqua"
	for i in grid_size():
		var b = _but.instantiate()
		b.index = i
		b.custom_minimum_size = Vector2i(32, 32)
		grid.add_child(b)
	update_all()
	for b in grid.get_children():
		b.disabled = false
	find_child(&"dbgadd").disabled = false

func update_all():
	swaps_label.text = str(swaps)
	for b in grid.get_children():
		b.ref = Patron.in_collection(Patron.cauldron, values[b.index]).key_name
		if swaps == 0:
			b.disabled = true
	if swaps == 0:
		for b in ingredients.get_children():
			b.disabled = true
		slime.disabled = true

func try_pay_ingredient(which: StringName, quantity: int = 4) -> void:
	var p
	match which:
		&"slime":
			p = slime.find_child(&"price_tag").price
		&"amanita":
			p = ingredients.get_node("Amanita").find_child(&"price_tag").price
		&"belladona":
			p = ingredients.get_node("Belladona").find_child(&"price_tag").price
		_:
			return
	if p.can_pay():
		p.pay()
		add_ingredient(which, quantity)

func add_ingredient(which: StringName, quantity: int = 4) -> void:
	#assert(which in _rev_sym)
	pop.play()
	var ids = []
	for i in len(values):
		if values[i] == &"alc_aqua":
			ids.push_back(i)
	ids.shuffle()
	var amount: int = min(len(ids), quantity)
	while amount > 0:
		values[ids[amount - 1]] = which
		amount -= 1
	check()

func dbgAdd() -> void:
	for ing in Patron.cauldron:
		values[ing] = ing
	update_all()

func swap(index, side):
	if side.call(index, grid_width(), grid_height()) != -1:
		swaps -= 1
		swaps_label.text = str(swaps)
		var bi = side.call(index, grid_width(), grid_height())
		var a = values[index]
		var b = values[bi]
		values[index] = b
		values[bi] = a
		check()

func check():
	#print(&"-- check --")
	var w := grid_width()
	var h := grid_height()
	var matches := {}
	var res := values.duplicate()
	for i in len(values):
		for d in Patron.potion_components:
			var c: Array = d.pattern.check(values, i, w, h)
			if not c.is_empty():
				if not i in matches:
					matches[i] =[]
				matches[i].push_back({&"matches": c, &"pattern": d.pattern, &"result": d.result})
	for i in matches:
		for pm in matches[i]:
			for m in pm.matches:
				var ps = pm.pattern.positions(m[1], m[2])
				#printt(i, p, m)
				for pos in ps:
					res[GridLimit.point2idx(GridLimit.idx2point(i, w, h) +\
							Vector2i(pos), w, h)] = pm.result
	values = res
	if not matches.is_empty():
		check()
	else:
		matches.clear()
		update_all()

func finish():
	GUI.beep.play()
	swaps = 0
	var counts = {}
	for i in len(values):
		var k = Patron.cauldron[values[i]]
		if not k in counts:
			counts[k] = 0
		counts[k] += 1
	#var relatives = {}
	#for k in counts:
		#relatives[k] = float(counts[k]) / float(grid_size())
	print(counts)
	ingredient_count = func(l: StringName) -> int: return counts[l] if l in counts else 0
	#print(relatives)
	var results = []
	for recipe in Patron.potion_recipes:
		if recipe.requirements.check():
			results.push_back(recipe.result)
	print(results)
	for i in len(values):
		values[i] = &"alc_aer"
	find_child(&"dbgadd").disabled = true
	update_all()

func _notification(what: int) -> void:
	if what == NOTIFICATION_MOUSE_ENTER:
		GUI.Tooltip.hide()
