class_name gTimer extends RefCounted

var date: float = 0.0

func complete():
	return Patron.date_passed(date)

func _init(t: float = 0.0):
	date = Patron.current_date() + t

func add(t: float):
	date += t

func reset(t: float):
	date = Patron.current_date() + t
