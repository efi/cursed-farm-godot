class_name MapGenerator extends Object

static func flip(a: Array[MapComponent], size: int, axis: int = 1) -> Array[MapComponent]:
	axis %= 3
	if axis == 0:
		return a
	var r: Array[MapComponent] = a.duplicate()
	for e in len(a):
		var n = GridLimit.flip(e, axis, size)
		r[e] = a[n]
		r[e].setindex(e, size)
	return r

static func rotat(a: Array[MapComponent], size: int, times: int = 1) -> Array[MapComponent]:
	times %= 4
	if times == 0:
		return a
	var r: Array[MapComponent] = a.duplicate()
	for e in len(a):
		var n = GridLimit.rotate(e, times, size)
		r[e] = a[n]
		r[e].setindex(e, size)
	return r

static func new_terrain(t: MapTemplate, size: int) -> Array[MapComponent]:
	return t.generate(size)

static var templates = {
	&"forest":
		MapTemplate.new(
			func(x, y, size):
				if		y > 0 and\
						y < size - 1 and\
						randf() < .6 - ((size - x - 4.0) / (size - 5.0)):
							return MapTemplate.FOREST
				elif	(randf() < .95 - (x - .8) / (size - 8.0)) or\
						(randf() < .95 - (size - y - .8) / (size - 9.0)):
							return MapTemplate.SEA
				elif	randf() < .8 - (y - 1.0) / (size - 10.0):
							return MapTemplate.MOUNTAINS
				else:
							return MapTemplate.PLAINS,
			func(map, size):
				return MapGenerator.flip(MapGenerator.rotat(map, size, randi_range(0, 3)), size, randi_range(0, 2))),
	&"mines":
		MapTemplate.new(
			func(_x, _y, _size):
				return MapTemplate.MINE_RESOURCES.pick_random(),
			func(map, _size):
				map.pick_random().add(MapTemplate.ENTRANCE)
				return map)
}
